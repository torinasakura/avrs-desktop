import fs from 'fs'
import path from 'path'
import mime from 'mime'
import { parse } from 'url'
import { protocol } from 'electron'
import { protocol as debug } from './debug'

class ProtocolHandler {
  register() {
    protocol.registerBufferProtocol('aversis', this.onRequest, this.onError)
  }

  onRequest({ url }, callback) {
    const filePath = path.join(path.dirname(process.mainModule.filename), 'public', parse(url).pathname)

    fs.readFile(filePath, (error, data) => {
      if (error) {
        debug(error)
        callback()
      } else {
        callback({ mimeType: mime.lookup(filePath), data })
      }
    })
  }

  onError(error) {
    debug(error)
  }
}

export default ProtocolHandler
