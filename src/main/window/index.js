import AuthWindow from './AuthWindow'
import MainWindow from './MainWindow'

export {
  AuthWindow,
  MainWindow,
}
