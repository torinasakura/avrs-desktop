import { BrowserWindow } from 'electron'

class Window {
  options = {}

  constructor() {
    this.protocol = process.env.NODE_ENV === 'development' ? 'http://localhost:3030/dist/' : 'aversis://aversis/'
  }

  getOptions() {
    if (process.env.NODE_ENV === 'development') {
      return {
        ...this.options,
        show: false,
        width: this.options.width ? this.options.width + 400 : this.options.width,
      }
    }

    return {
      ...this.options,
      show: false,
    }
  }

  open(pathname) {
    this.browserWindow = new BrowserWindow(this.getOptions())
    this.browserWindow.loadURL(`${this.protocol}${pathname}`)

    this.browserWindow.once('closed', this.onClosed)

    return new Promise((resolve) => {
      this.browserWindow.webContents.once('did-finish-load', () => {
        this.browserWindow.show()
        this.browserWindow.focus()

        if (process.env.NODE_ENV === 'development') {
          this.browserWindow.openDevTools()
        }

        resolve()
      })
    })
  }

  hide() {
    this.browserWindow.hide()
  }

  close() {
    this.browserWindow.close()
  }

  send(...args) {
    this.browserWindow.webContents.send(...args)
  }

  isMinimized() {
    return this.browserWindow && this.browserWindow.isMinimized()
  }

  isClosed() {
    return !this.browserWindow
  }

  restore() {
    return this.browserWindow.restore()
  }

  onClosed = () => {
    this.browserWindow = null
  }
}

export default Window
