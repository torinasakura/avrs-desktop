import Window from './Window'

class MainWindow extends Window {
  options = {
    width: 900,
    height: 600,
    minWidth: 900,
    minHeight: 550,
    frame: false,
  }

  open() {
    return super.open('main.html')
  }
}

export default MainWindow
