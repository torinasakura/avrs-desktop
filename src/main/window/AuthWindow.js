import Window from './Window'

class AuthWindow extends Window {
  options = {
    width: 400,
    height: 500,
    resizable: false,
  }

  open() {
    return super.open('auth.html')
  }
}

export default AuthWindow
