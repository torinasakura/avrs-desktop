import path from 'path'
import debug from 'debug'
import { app } from 'electron'
import { Logger, transports } from 'winston'

if (process.env.NODE_ENV !== 'development') {
  const logger = new (Logger)({
    transports: [
      new (transports.File)({ filename: path.join(path.dirname(app.getPath('exe')), 'aversis.log') }),
    ],
  })

  debug.log = logger.info
}

debug.enable('*')

export const exception = debug('avrs:exception')
export const application = debug('avrs:application')
export const protocol = debug('avrs:protocol')
export const settings = debug('avrs:settings')
export const updateManager = debug('avrs:update-manager')
export const api = debug('avrs:api')
export const squirrel = debug('avrs:squirrel')
export const tray = debug('avrs:tray')
