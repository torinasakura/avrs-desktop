import { app } from 'electron'
import ProtocolHandler from './ProtocolHandler'
import IpcManager from './IpcManager'
import Settings from './Settings'
import ApiClient from './ApiClient'
import Tray from './Tray'
import Scheduler from './scheduler/Scheduler'
import { AuthWindow, MainWindow } from './window'

class Application {
  constructor() {
    this.protocolHandler = new ProtocolHandler()
    this.ipcManager = new IpcManager()
    this.apiClient = new ApiClient()
    this.settings = new Settings()
    this.tray = new Tray()
    this.scheduler = new Scheduler()

    this.windows = {
      auth: new AuthWindow(),
      main: new MainWindow(),
    }

    this.ipcManager.on('login', this.onLogin)
    this.ipcManager.on('logout', this.onLogout)
    this.ipcManager.on('schedule:save', this.onScheduleSave)
    this.ipcManager.on('session:start', this.onSessionStart)
    this.ipcManager.on('session:stop', this.onSessionStop)

    this.apiClient.on('logout', this.onLogout)

    this.tray.on('open', this.onOpenPage)
    this.tray.on('quit', this.quit)

    this.scheduler.on('stat', this.onSessionStat)
    this.scheduler.on('started', this.onSessionStarted)
    this.scheduler.on('stopped', this.onSessionStoped)
    this.scheduler.on('tick', this.onSessionTick)
    this.scheduler.on('wait', this.onSessionWait)

    app.on('window-all-closed', this.onWindowAllClosed)
  }

  async start() {
    this.protocolHandler.register()
    this.ipcManager.init()

    await this.settings.load()

    await this.launch()
  }

  async launch() {
    if (!this.settings.getToken()) {
      await this.windows.auth.open()

      return null
    }

    await this.windows.main.open()

    this.apiClient.setToken(this.settings.getToken())

    const { user, servicePlans } = await this.apiClient.load()

    this.settings.setUser(user)
    this.settings.setServicePlans(servicePlans)

    await this.settings.loadMachineId()

    if (this.settings.isActivated()) {
      await this.settings.sync()
    }

    this.windows.main.send('init', this.settings.toJSON())

    if (this.settings.isActivated()) {
      this.tray.add()

      if (this.settings.isStarted()) {
        this.scheduler.setSchedules(this.settings.getSchedules())
        this.scheduler.setMachineId(this.settings.getMachineId())
        this.scheduler.setToken(this.settings.getToken())
        await this.scheduler.start()
      }
    }

    return null
  }

  quit() {
    app.quit()
  }

  onWindowAllClosed = () => {
    if (!this.settings.isActivated()) {
      this.quit()
    }
  }

  onLogin = async (token) => {
    this.settings.setToken(token)
    await this.settings.save()
    await this.windows.auth.hide()
    await this.launch()
    await this.windows.auth.close()
  }

  onLogout = async () => {
    this.settings.setToken(null)
    await this.settings.save()
    await this.windows.main.hide()
    await this.launch()
    await this.windows.main.close()

    this.scheduler.stop()
    this.tray.destroy()
  }

  onOpenPage = async (target) => {
    if (this.windows.main.isClosed()) {
      await this.windows.main.open()
      this.windows.main.send('init', this.settings.toJSON())
    }

    if (this.windows.main.isMinimized()) {
      this.windows.main.restore()
    }

    this.windows.main.send('location:change', target)
  }

  onScheduleSave = async (schedules) => {
    this.settings.setSchedules(schedules)
    await this.settings.save()

    if (this.settings.isStarted()) {
      this.scheduler.setSchedules(schedules)
      this.scheduler.setMachineId(this.settings.getMachineId())
      this.scheduler.setToken(this.settings.getToken())
      this.scheduler.refresh()
    }

    this.windows.main.send('schedule:saved')
  }

  onSessionStart = async () => {
    this.settings.setStarted(true)
    await this.settings.save()

    this.scheduler.setSchedules(this.settings.getSchedules())
    this.scheduler.setMachineId(this.settings.getMachineId())
    this.scheduler.setToken(this.settings.getToken())
    await this.scheduler.start()
  }

  onSessionStop = async () => {
    this.settings.setStarted(false)
    await this.settings.save()
    this.scheduler.stop()
  }

  onSessionStat = (data) => {
    this.windows.main.send('session:stat', data)
  }

  onSessionStarted = (data) => {
    this.windows.main.send('session:started', data)
  }

  onSessionStoped = () => {
    this.windows.main.send('session:stopped')
  }

  onSessionTick = (data) => {
    this.windows.main.send('session:tick', data)
  }

  onSessionWait = (data) => {
    this.windows.main.send('session:wait', data)
  }
}

export default Application
