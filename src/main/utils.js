import moment from 'moment'

export const generateSchedule = (time) => {
  const startTime = moment().startOf('day').add(Math.floor((24 - time) / 2), 'hour')
  const dayValues = Array.from(Array(time).keys()).map(() => startTime.add(1, 'hour').format('HH:mm'))

  return {
    mon: dayValues,
    tue: dayValues,
    wed: dayValues,
    thu: dayValues,
    fri: dayValues,
    sat: dayValues,
    sun: dayValues,
  }
}

export const generateEmptySchedule = () => ({ mon: [], tue: [], wed: [], thu: [], fri: [], sat: [], sun: [] })
