import path from 'path'
import { EventEmitter } from 'events'
import { Tray as ElectronTray, Menu, nativeImage } from 'electron'
import logoIcon from './../../resources/logo.png'
import { tray as debug } from './debug'

class Tray extends EventEmitter {
  getIcon() {
    return nativeImage.createFromPath(this.getIconPath(logoIcon))
  }

  getIconPath(icon) {
    if (process.env.NODE_ENV === 'development') {
      return path.join(path.dirname(process.mainModule.filename), '..', 'resources', icon)
    }

    return path.join(path.dirname(process.mainModule.filename), icon)
  }

  onQuit = () => {
    this.emit('quit')
  }

  onStatistics = () => {
    this.emit('open', 'statistics')
  }

  onServices = () => {
    this.emit('open', 'services')
  }

  onProfile = () => {
    this.emit('open', 'profile')
  }

  getContextMenu() {
    return Menu.buildFromTemplate([
      {
        label: 'Statistics',
        click: this.onStatistics,
      },
      {
        label: 'Profile',
        click: this.onProfile,
      },
      {
        type: 'separator',
      },
      {
        label: 'Quit Aversis',
        click: this.onQuit,
      },
    ])
  }

  add() {
    if (this.tray) {
      return
    }

    try {
      this.tray = new ElectronTray(this.getIcon())
      this.tray.setContextMenu(this.getContextMenu())
    } catch (error) {
      debug(error)
    }
  }

  destroy() {
    if (this.tray) {
      this.tray.destroy()
      this.tray = null
    }
  }
}

export default Tray
