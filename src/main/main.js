import { app } from 'electron'
import { exception as debug } from './debug'
import startCrashReporter from './startCrashReporter'
import { handleStartupEventWithSquirrel } from './squirrelUpdate'

app.commandLine.appendSwitch('ignore-certificate-errors', 'true')

export default function run() {
  if (handleStartupEventWithSquirrel()) {
    return
  }

  process.on('uncaughtException', ({ message, stack } = {}) => {
    if (message !== null) {
      debug(message)
    }

    if (stack !== null) {
      debug(stack)
    }
  })

  app.on('will-finish-launching', startCrashReporter)

  app.on('ready', () => {
    const AversisApplication = require('./Application').default // eslint-disable-line global-require
    const aversis = new AversisApplication()
    aversis.start()
  })
}
