import { app } from 'electron'
import { spawn } from 'child_process'
import path from 'path'
import { squirrel as debug } from './debug'

const spawnUpdate = (args, callback) => {
  const updateExe = path.resolve(path.dirname(process.execPath), '..', 'Update.exe')

  debug('Spawning `%s` with args `%s`', updateExe, args)

  spawn(updateExe, args, { detached: true }).on('close', callback)
}

const onCompleteSpawn = (error, stdout) => {
  if (error) {
    debug(error)
  }

  debug(stdout)

  app.quit()
}

export function handleStartupEventWithSquirrel() {
  if (process.platform === 'win32') {
    const cmd = process.argv[1]

    debug('processing squirrel command `%s`', cmd)

    const target = path.basename(process.execPath)

    if (cmd === '--squirrel-install' || cmd === '--squirrel-updated') {
      spawnUpdate([`--createShortcut=${target}`], onCompleteSpawn)
      return true
    }

    if (cmd === '--squirrel-uninstall') {
      spawnUpdate([`--removeShortcut=${target}`], onCompleteSpawn)
      return true
    }

    if (cmd === '--squirrel-obsolete') {
      app.quit()
      return true
    }
  }

  return false
}
