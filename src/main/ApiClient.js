/* eslint-disable no-param-reassign */
import 'isomorphic-fetch'
import { EventEmitter } from 'events'
import ApolloClient, { createNetworkInterface } from 'apollo-client'
import gql from 'graphql-tag'

class ApiClient extends EventEmitter {
  constructor() {
    super()

    const networkInterface = createNetworkInterface({ uri: process.env.API_URL })

    networkInterface.use([{
      applyMiddleware: this.applyMiddleware,
    }])

    networkInterface.useAfter([{
      applyAfterware: this.applyAfterware,
    }])

    this.client = new ApolloClient({
      networkInterface,
    })
  }

  applyMiddleware = (req, next) => {
    const token = this.token

    if (!req.options.headers) {
      req.options.headers = {}
    }

    if (token) {
      req.options.headers.authorization = token
    }

    next()
  }

  applyAfterware = ({ response }, next) => {
    if (response.status === 401) {
      this.emit('logout')
    }

    next()
  }

  setToken(token) {
    this.token = token
  }

  async load() {
    const { data } = await this.client.query({
      query: gql`
        query {
          user {
            id
            email
            firstName
            lastName
            balance
            salesBalance
            inviteCode
            referals
            status
            activations {
              id
              status
              startAt
              leftTime
              createdAt
              servicePlan {
                type
                period
                time
              }
            }
          }
          servicePlans {
            type
            period
            time
            price
            profitabilityPerDay
            profitabilityPerHour
            amount
            memory
            cpu
          }
        }
      `,
    })

    return data
  }
}

export default ApiClient
