export default (extra) => {
  const { crashReporter } = require('electron') // eslint-disable-line global-require

  crashReporter.start({
    productName: 'Aversis',
    companyName: 'Aversis',
    submitURL: 'https://crashreporter.aversis.net',
    autoSubmit: false,
    extra,
  })
}
