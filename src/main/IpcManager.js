import { EventEmitter } from 'events'
import { ipcMain } from 'electron'

class IpcManager extends EventEmitter {
  init() {
    ipcMain.on('auth:login', this.onLogin)
    ipcMain.on('auth:logout', this.onLogout)
    ipcMain.on('schedule:save', this.onScheduleSave)
    ipcMain.on('session:start', this.onStart)
    ipcMain.on('session:stop', this.onStop)
  }

  onLogin = (event, { token }) => {
    this.emit('login', token)
  }

  onLogout = () => {
    this.emit('logout')
  }

  onScheduleSave = (event, data) => {
    this.emit('schedule:save', data)
  }

  onStart = () => {
    this.emit('session:start')
  }

  onStop = () => {
    this.emit('session:stop')
  }
}

export default IpcManager
