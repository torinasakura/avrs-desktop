import { EventEmitter } from 'events'
import moment from 'moment'
import { powerSaveBlocker } from 'electron'
import Session from './Session'
import { formatSchedules, searchMatchedSchedule, searchNextMatchedSchedule, random } from './utils'

class Scheduler extends EventEmitter {
  checkTimeout = 1000

  onCheck = async () => {
    if (this.session) {
      if (this.session.endAt <= (new Date()).getTime()) {
        this.refresh()

        return null
      }

      const currentTime = Math.floor(((new Date()).getTime() - this.session.startAt) / 1000)

      if ((currentTime % 4) === 0) {
        const data = {
          c: random(4, 10),
          m: random(400, 1000),
        }

        if (this.session.started) {
          this.emit('stat', data)
        }
      }
    }

    if (this.nextStartAt && this.nextStartAt <= (new Date()).getTime()) {
      this.startSession()
    }

    this.emit('tick')

    return null
  }

  onStart = (data) => {
    this.emit('started', data)
  }

  setSchedules(schedules) {
    this.schedules = schedules
  }

  setToken(token) {
    this.token = token
  }

  setMachineId(machineId) {
    this.machineId = machineId
  }

  searchCurrent() {
    const today = moment().format('ddd').toLowerCase()
    const now = (new Date()).getTime()

    const [matched] = searchMatchedSchedule(formatSchedules(this.schedules), today, now)

    return matched
  }

  searchNext() {
    const today = moment().format('ddd').toLowerCase()
    const now = (new Date()).getTime()

    const [matched] = searchNextMatchedSchedule(formatSchedules(this.schedules), today, now)

    return matched
  }

  async start() {
    this.interval = setInterval(this.onCheck, this.checkTimeout)
    this.powerSaveBlockerId = powerSaveBlocker.start('prevent-app-suspension')

    this.startSession()
  }

  async refresh() {
    this.closeSession()
    this.startSession()
  }

  async stop() {
    this.closeSession()

    if (this.interval) {
      clearInterval(this.interval)
    }

    if (this.powerSaveBlockerId) {
      powerSaveBlocker.stop(this.powerSaveBlockerId)
    }

    this.emit('stopped')
  }

  async startSession() {
    const current = this.searchCurrent()
    this.nextStartAt = null

    if (current) {
      this.activation = current.id
      this.endTime = current.to

      this.session = new Session(this.token, this.machineId, current.id, current.from, current.to)
      this.session.start()
      this.session.on('started', this.onStart)
    } else {
      const next = this.searchNext()

      if (next) {
        this.nextStartAt = next.from
        this.emit('wait', { activation: next.id, nextStartAt: next.from })
      }
    }
  }

  async closeSession() {
    if (this.session) {
      this.session.removeListener('started', this.onStart)
      this.session.close()
      this.session = null
    }
  }
}

export default Scheduler
