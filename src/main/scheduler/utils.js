/* eslint-disable no-mixed-operators */
/* eslint-disable no-param-reassign */
/* eslint-disable arrow-body-style */
/* eslint-disable no-shadow */
import moment from 'moment'

const daysRanges = (from = moment()) =>
  Array.from(Array(7).keys()).map(day => moment(from).add(day, 'day').format('ddd').toLowerCase())

export const random = (min, max) => Math.floor(Math.random() * ((max - min) + 1) + min)

export const hourToTime = (hour, day) =>
  moment()
    .startOf('day')
    .add(hour, 'hour')
    .add(day, 'day')
    .valueOf()

export const groupScheduleHours = (hours) => {
  const { intervals } = hours.reduce((result, hour, index) => {
    const current = hour
    const currentInterval = (result.intervals[result.current] || []).concat([current])
    const next = hours[index + 1]

    result.intervals[result.current] = currentInterval

    return {
      ...result,
      current: next && (next - current) > 1 ? result.current + 1 : result.current,
    }
  }, { intervals: [], current: 0 })

  return intervals
}

export const convertHoursGroupToInterval = (group, day) => {
  if (group.length === 1) {
    return {
      from: hourToTime(group[0], day),
      to: hourToTime(group[0] + 1, day),
    }
  }

  return {
    from: hourToTime(group[0], day),
    to: hourToTime(group[group.length - 1] + 1, day),
  }
}

export const fourmatHours = hours => hours.map(hour => Number(hour.split(':').shift())).sort((a, b) => a > b)

export const formatScheduleDays = days =>
  daysRanges().reduce((result, day, index) => ({
    ...result,
    [day]: groupScheduleHours(fourmatHours(days[day])).map(group => convertHoursGroupToInterval(group, index)),
  }), {})

export const formatSchedules = schedules =>
  Object.keys(schedules).reduce((result, id) => ({
    ...result,
    [id]: formatScheduleDays(schedules[id].schedule),
  }), {})

export const searchMatchedSchedule = (schedules, day, time) =>
  Object.keys(schedules).reduce((result, id) => {
    const [interval] = schedules[id][day].filter(({ from, to }) => from <= time && to >= time)

    if (interval) {
      return [
        ...result,
        { id, ...interval },
      ]
    }

    return result
  }, [])

export const searchNextMatchedSchedule = (schedules, fromDay, fromTime) => {
  const daysRange = daysRanges()
  const currentDayIndex = daysRange.indexOf(fromDay)
  const daysSequence = daysRange.slice(daysRange.indexOf(currentDayIndex), daysRange.length)
                                .concat(daysRange.slice(0, daysRange.indexOf(currentDayIndex)))

  const intervals = Object.keys(schedules).reduce((result, id) => { // eslint-disable-line no-unused-vars
    return daysSequence.reduce((result, day) =>
      result.concat(Object.keys(schedules).reduce((result, id) =>
        result.concat(schedules[id][day].filter(({ from }) => from >= fromTime).map(schedule => ({ ...schedule, id })))
      , []))
    , [])
  }, [])

  return intervals.sort((a, b) => a.from - b.from)
}
