import { EventEmitter } from 'events'
import sio from 'socket.io-client'
import qs from 'querystring'

class Session extends EventEmitter {
  constructor(token, machineId, activation, startAt, endAt) {
    super()

    const now = (new Date()).getTime()

    this.token = token
    this.machineId = machineId
    this.activation = activation
    this.startAt = ((now - startAt) / 1000) > 30 ? now : startAt
    this.endAt = endAt
  }

  onConnect = () => {
    if (this.started) {
      this.socket.emit('session:restore', { activationId: this.activation })
    }
  }

  onError = (error) => {
    console.log(error) // eslint-disable-line no-console
    console.log('error') // eslint-disable-line no-console
  }

  onStarted = () => {
    this.started = true

    this.emit('started', {
      activation: this.activation,
      startAt: this.startAt,
      endAt: this.endAt,
    })
  }

  start() {
    this.socket = sio(process.env.API_URL, {
      transports: ['websocket', 'polling'],
      query: qs.stringify({
        type: 'agent',
        token: this.token,
        activation: this.activation,
        startAt: this.startAt,
        machineId: this.machineId,
      }),
    })

    this.socket.on('connect', this.onConnect)
    this.socket.on('error', this.onError)

    this.socket.on('session:started', this.onStarted)
    this.socket.emit('session:start', { startAt: this.startAt, activationId: this.activation })
  }

  close() {
    this.socket.disconnect()
    this.socket.destroy()
    this.socket = null
  }
}

export default Session
