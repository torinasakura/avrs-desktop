import storage from 'electron-storage'
import { machineId } from 'node-machine-id'
import { generateSchedule, generateEmptySchedule } from './utils'
import { settings as debug } from './debug'

class Settings {
  settings = {
    started: false,
    schedules: {},
  }

  user = {}

  servicePlans = []

  setStarted(started) {
    this.settings.started = started
  }

  getToken() {
    return this.settings.token
  }

  setToken(token) {
    this.settings.token = token
  }

  getUser() {
    return this.user
  }

  setUser(user) {
    this.user = user
  }

  setServicePlans(servicePlans) {
    this.servicePlans = servicePlans
  }

  setSchedules(schedules) {
    this.settings.schedules = schedules
  }

  getSchedules() {
    return this.settings.schedules
  }

  getMachineId() {
    return this.machineId
  }

  isStarted() {
    return this.settings.started
  }

  isActivated() {
    if (!(this.user && this.user.status)) {
      return false
    }

    return this.user.status !== 'NEW' && this.user.status !== 'NOT_ACTIVATED'
  }

  toJSON() {
    const { activationId, schedules, started, token } = this.settings

    return JSON.stringify({
      servicePlans: this.servicePlans,
      user: this.user,
      schedules,
      schedule: schedules[activationId],
      activationId,
      started,
      token,
    })
  }

  async sync() {
    const updates = {
      ...this.fillSchedules(),
      ...this.setActication(),
      ...this.clearActivation(),
    }

    if (Object.keys(updates).length > 0) {
      this.settings = {
        ...this.settings,
        ...updates,
      }

      await this.save()
    }
  }

  fillSchedules() {
    const { activations } = this.user

    const activationsIds = activations.map(({ id }) => id, [])

    const schedules = Object.keys(this.settings.schedules).reduce((result, id) => {
      if (!activationsIds.includes(id)) {
        return result
      }

      return { ...result, [id]: this.settings.schedules[id] }
    }, {})

    const newSchedules = this.user.activations.reduce((result, { id, servicePlan }, index) => {
      if (!(schedules && schedules[id])) {
        return {
          ...result,
          [id]: {
            schedule: index === 0 ? generateSchedule(servicePlan.time) : generateEmptySchedule(),
          },
        }
      }

      return result
    }, {})

    if (Object.keys(newSchedules).length > 0) {
      return { schedules: newSchedules }
    }

    return {}
  }

  setActication() {
    const { activationId } = this.settings

    if (!activationId && this.user.activations.length > 0) {
      return { activationId: this.user.activations[0].id }
    }

    return {}
  }

  clearActivation() {
    const { activationId } = this.settings

    if (activationId && !this.user.activations.some(({ id }) => id === activationId)) {
      return { activationId: null }
    }

    return {}
  }

  async load() {
    try {
      this.settings = await storage.get('settings')
    } catch (error) {
      debug(error)
    }
  }

  async save() {
    try {
      await storage.set('settings', this.settings)
    } catch (error) {
      debug(error)
    }
  }

  async clear() {
    try {
      await storage.set('settings', {})
    } catch (error) {
      debug(error)
    }
  }

  async loadMachineId() {
    this.machineId = await machineId()
  }
}

export default Settings
