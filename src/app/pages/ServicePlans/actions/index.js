import * as actions from '../constants'

export function select(plan, period) {
  return {
    type: actions.select,
    plan,
    period,
  }
}

export function changePeriod(period) {
  return {
    type: actions.changePeriod,
    period,
  }
}

export function changeTime(time) {
  return {
    type: actions.changeTime,
    time,
  }
}

export function changeCPU(cpu) {
  return {
    type: actions.changeCPU,
    cpu,
  }
}

export function changeMemory(memory) {
  return {
    type: actions.changeMemory,
    memory,
  }
}
