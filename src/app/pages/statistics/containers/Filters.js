import { connect } from 'react-redux'
import { toggle, changePeriod, changeActivation, load } from '../actions/filters'
import Filters from '../components/filters/Filters'

export default connect(
  state => ({
    show: state.statistics.filters.show,
    period: state.statistics.filters.period,
    activations: state.statistics.filters.activations,
  }),
  dispatch => ({
    onLoad: () => dispatch(load()),
    onToggle: () => dispatch(toggle()),
    onChangePeriod: value => dispatch(changePeriod(value)),
    onChangeActivation: (id, value) => dispatch(changeActivation(id, value)),
  }),
)(Filters)
