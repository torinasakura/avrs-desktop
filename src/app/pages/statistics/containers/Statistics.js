import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Text } from 'avrs-ui/src/text'
import Filters from './Filters'
import Sessions from './Sessions'

const Statistics = () => (
  <Row>
    <Layout overflow>
      <Filters />
    </Layout>
    <Layout basis='15px' />
    <Layout>
      <Column align='center'>
        <Layout basis='35px' />
        <Layout>
          <Text size='large' weight='light'>
            Статистика
          </Text>
        </Layout>
        <Layout grow={1} />
      </Column>
    </Layout>
    <Layout grow={1}>
      <Column>
        <Layout basis='35px' />
        <Layout grow={1}>
          <Sessions />
        </Layout>
        <Layout basis='35px' />
      </Column>
    </Layout>
  </Row>
)

export default Statistics
