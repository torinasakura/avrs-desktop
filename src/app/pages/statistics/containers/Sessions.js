import { connect } from 'react-redux'
import Sessions from '../components/sessions/Sessions'

export default connect(
  state => ({
    list: state.statistics.sessions.list,
    chart: state.statistics.sessions.chart,
  }),
)(Sessions)
