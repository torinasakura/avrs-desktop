import moment from 'moment'
import { createReducer } from '../../../../utils'
import * as actions from '../../constants/sessions'

const initialState = []

const colors = ['#00BB27', '#0288D1']

export default createReducer(initialState, {
  [actions.load]: (state, { sessions, from, to, activations }) => {
    const colorsByActivation = activations.reduce((result, id, index) => ({
      ...result,
      [id]: colors[index],
    }), {})

    const getColor = id => colorsByActivation[parseInt(id, 10)] || '#00BB27'

    const sessionsByDays = sessions.reduce((result, session) => {
      const date = moment(session.startAt).startOf('day').toDate()

      if (!result[date]) {
        result[date] = [] // eslint-disable-line no-param-reassign
      }

      result[date].push({
        ...session,
        date: session.startAt,
        value: session.time,
        color: getColor(session.activation.id),
      })

      return result
    }, {})

    const startAt = moment(new Date(from)).subtract(1, 'day')
    const period = moment(new Date(to)).diff(startAt, 'days') + 1

    return Array.from(Array(period).keys()).reduce((result, day) => {
      const date = moment(startAt).add(day, 'day').startOf('day').toDate()

      if (!sessionsByDays[date]) {
        return [
          ...result,
          { date, value: 0 },
        ]
      }

      return [
        ...result,
        ...sessionsByDays[date],
      ]
    }, [])
  },
})
