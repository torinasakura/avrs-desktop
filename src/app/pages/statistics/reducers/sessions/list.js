import moment from 'moment'
import { createReducer } from '../../../../utils'
import * as actions from '../../constants/sessions'

const initialState = []

const names = {
  basis: 'Базис',
  standart: 'Стандарт',
  premium: 'Премиум',
  business: 'Бизнес',
}

export default createReducer(initialState, {
  [actions.load]: (state, { sessions }) => sessions.map(session => ({
    ...session,
    startAt: moment(new Date(session.startAt)).format('DD MMMM, HH:mm'),
    endAt: moment(new Date(session.startAt)).add(session.time, 'second').format('HH:mm'),
    time: moment().startOf('day').seconds(session.time).format('H:mm:ss'),
    name: names[session.activation.servicePlan.type],
  })),
})
