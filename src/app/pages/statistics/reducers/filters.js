import moment from 'moment'
import { createReducer } from '../../../utils'
import * as actions from '../constants/filters'

const initialState = {
  show: false,
  activations: [],
  period: {
    from: moment().subtract(1, 'month').toDate(),
    to: moment().toDate(),
  },
}

export default createReducer(initialState, {
  [actions.sync]: (state, { activations }) => ({
    ...state,
    activations: activations.map(activation => ({ ...activation, selected: true })),
  }),
  [actions.toggle]: state => ({ ...state, show: !state.show }),
  [actions.changePeriod]: (state, { value }) => ({
    ...state,
    period: value,
  }),
  [actions.changeActivation]: (state, { id, value }) => ({
    ...state,
    activations: state.activations.map((activation) => {
      if (activation.id === id) {
        return { ...activation, selected: value }
      }

      return activation
    }),
  }),
})
