import { combineReducers } from 'redux'
import filters from './filters'
import sessions from './sessions'

export default combineReducers({
  filters,
  sessions,
})
