import { load } from '../actions/sessions'
import Statistics from '../containers/Statistics'

export default ({ dispatch }) => ({
  path: 'statistics',
  component: Statistics,
  onEnter() {
    dispatch(load())
  },
})
