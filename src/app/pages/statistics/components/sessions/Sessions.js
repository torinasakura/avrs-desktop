import React from 'react'
import { Row, Layout } from 'flex-layouts'
import { Condition } from 'avrs-ui/src/condition'
import { BarChart } from 'avrs-ui/src/chart'
import { AutoSizer } from 'avrs-ui/src/content'
import { Container, Row as ListRow, Cell } from './list'

const Sessions = ({ list = [], chart = [] }) => (
  <Row>
    <Layout basis='15px' />
    <Layout basis='180px'>
      <Condition match={chart.length > 0}>
        <AutoSizer>
          <BarChart
            data={chart}
          />
        </AutoSizer>
      </Condition>
    </Layout>
    <Layout basis='10px' />
    <ListRow>
      <Cell borderTop borderLeft basis='25%'>
        Начало
      </Cell>
      <Cell borderTop borderLeft basis='25%'>
        Завершение
      </Cell>
      <Cell borderTop borderLeft basis='25%'>
        Время
      </Cell>
      <Cell borderTop borderLeft borderRight basis='25%'>
        Тариф
      </Cell>
    </ListRow>
    <Layout grow={1}>
      <Condition match={list.length > 0}>
        <Container>
          {list.map((session, index) => (
            <ListRow key={index}>
              <Cell basis='25%' borderTop={index > 0} borderLeft>
                {session.startAt}
              </Cell>
              <Cell basis='25%' borderTop={index > 0} borderLeft>
                {session.endAt}
              </Cell>
              <Cell basis='25%' borderTop={index > 0} borderLeft>
                {session.time}
              </Cell>
              <Cell basis='25%' borderTop={index > 0} borderLeft borderRight >
                {session.name}
              </Cell>
            </ListRow>
          ))}
        </Container>
      </Condition>
      <Condition match={list.length === 0}>
        <Container>
          <ListRow>
            <Cell basis='100%' align='center' borderRight borderBottom borderLeft>
              Нет данных
            </Cell>
          </ListRow>
        </Container>
      </Condition>
    </Layout>
    <Layout basis='20px' />
  </Row>
)

export default Sessions
