import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    padding: '10px',
    boxSizing: 'border-box',
    fontSize: '12px',
    color: '#505458',
    fontFamily: '"Ubuntu", sans-serif',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  borderTop: {
    borderTop: '1px solid #E5E5E5',
  },
  borderRight: {
    borderRight: '1px solid #E5E5E5',
  },
  borderBottom: {
    borderBottom: '1px solid #E5E5E5',
  },
  borderLeft: {
    borderLeft: '1px solid #E5E5E5',
  },
  'align=center': {
    justifyContent: 'center',
  },
})

const Cell = ({ children, borderTop, borderRight, borderBottom, borderLeft, basis, align }) => (
  <div
    style={{ flex: `0 0 ${basis}` }}
    className={styles({ borderTop, borderRight, borderBottom, borderLeft, align })}
  >
    {children}
  </div>
)

export default Cell
