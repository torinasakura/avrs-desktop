import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    maxWidth: '100%',
  },
})

const Row = ({ children }) => (
  <div className={styles({ })}>
    {children}
  </div>
)

export default Row
