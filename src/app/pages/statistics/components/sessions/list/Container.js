import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {},
  container: {
    width: '100%',
    minHeight: '100%',
    maxHeight: '100%',
    overflowY: 'auto',
    position: 'relative',
  },
  wrapper: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflowY: 'auto',
    overflowX: 'hidden',
    borderTop: '1px solid #e5e5e5',
    borderBottom: '1px solid #e5e5e5',
    boxSizing: 'border-box',
  },
})

const Container = ({ children }) => (
  <div className={styles({ container: true })}>
    <div className={styles({ wrapper: true })}>
      {children}
    </div>
  </div>
)

export default Container
