import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    width: '110%',
    height: '20px',
    boxShadow: 'inset 0 1px 2px 0px rgba(0, 0, 0, 0.15)',
    borderTop: '1px solid #E1E4E6',
    transition: 'opacity 2s',
    opacity: 0,
  },
  show: {
    opacity: 1,
  },
})

const Divider = ({ show }) => (
  <div className={styles({ show })} />
)

export default Divider
