import React from 'react'
import { StyleSheet } from 'elementum'
import Icon from 'avrs-ui/src/icons/Icon'
import { rotate } from 'avrs-ui/src/icons/utils'

const styles = StyleSheet.create({
  self: {
    position: 'absolute',
    border: '1px solid #0288d1',
    borderRadius: '50%',
    padding: '6px 8px 4px 8px',
    cursor: 'pointer',
    top: '15px',
    right: '20px',
    zIndex: 3,
    background: '#ffffff',
    '& svg': {
      fill: '#0288d1',
    },
    '&:hover': {
      opacity: 0.9,
    },
  },
})

const ArrowIcon = ({ right, down, left, ...props }) => (
  <Icon originalWidth={24} originalHeight={24} {...props}>
    <g transform={rotate({ right, down, left }, 24, 24)}>
      <path
        d='M12 2q0.422 0 0.711 0.289l7 7q0.289 0.289 0.289 0.711
          0 0.43-0.285 0.715t-0.715 0.285q-0.422 0-0.711-0.289l-5.289-5.297v15.586q0
          0.414-0.293 0.707t-0.707 0.293-0.707-0.293-0.293-0.707v-15.586l-5.289
          5.297q-0.289 0.289-0.711 0.289-0.43 0-0.715-0.285t-0.285-0.715q0-0.422
          0.289-0.711l7-7q0.289-0.289 0.711-0.289z'
      />
    </g>
  </Icon>
)

const Expander = ({ inverse, onClick }) => (
  <div
    className={styles()}
    onClick={onClick}
  >
    <ArrowIcon
      height={12}
      down={!inverse}
    />
  </div>
)

export default Expander
