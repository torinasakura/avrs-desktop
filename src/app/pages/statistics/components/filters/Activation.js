import React from 'react'
import moment from 'moment'
import { StyleSheet } from 'elementum'
import { Checkbox } from 'avrs-ui/src/checkbox'

const names = {
  basis: 'Базис',
  standart: 'Стандарт',
  premium: 'Премиум',
  business: 'Бизнес',
}

const styles = StyleSheet.create({
  self: {
    fontSize: '14px',
    fontWeight: 300,
    fontFamily: '"Ubuntu", sans-serif',
    lineHeight: '12px',
    cursor: 'pointer',
    padding: '0px 0px 12px 0px',
    background: 'transparent',
    textDecoration: 'none',
    outline: 0,
    color: 'black',
    '& span': {
      fontSize: '12px',
    },
    '& div': {
      marginRight: '8px',
    },
  },
})

const formatCreatedAt = createdAt => moment(new Date(createdAt)).format('LL')

const Activation = ({ selected, id, createdAt, servicePlan, onChange }) => (
  <div
    className={styles()}
    onClick={() => onChange(id, !selected)}
  >
    <Checkbox
      type='desktop'
      value={selected}
      onChange={f => f}
    />
    {names[servicePlan.type]} #{id}, {formatCreatedAt(createdAt)}
  </div>
)

export default Activation
