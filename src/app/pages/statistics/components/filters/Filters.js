import React from 'react'
import { StyleSheet } from 'elementum'
import { Column, Row, Layout } from 'flex-layouts'
import { GhostButton } from 'avrs-ui/src/button'
import { Divider } from 'avrs-ui/src/divider'
import { Text } from 'avrs-ui/src/text'
import RangeCalendar from 'avrs-ui/src/datepicker/RangeCalendar'
import Container from './Container'
import Activation from './Activation'
import ContentDivider from './Divider'
import Expander from './Expander'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    width: '100%',
  },
})

const Filters = ({ show, activations = [], period = {}, onChangePeriod, onChangeActivation, onToggle, onLoad }) => (
  <div className={styles()}>
    <Expander
      inverse={show}
      onClick={onToggle}
    />
    <Container show={show}>
      <div style={{ background: '#ffffff' }}>
        <Row>
          <Layout basis='18px' />
          <Layout>
            <Column>
              <Layout basis='35px' />
              <Layout>
                <Row>
                  <Layout>
                    <Text size='small'>
                      Период
                    </Text>
                  </Layout>
                  <Layout basis='20px' />
                  <Layout>
                    <RangeCalendar
                      locale='ru'
                      from={period.from}
                      to={period.to}
                      numberOfMonths={2}
                      onChange={onChangePeriod}
                    />
                  </Layout>
                </Row>
              </Layout>
              <Layout basis='40px' />
              <Layout>
                <Row>
                  <Layout basis='30px' />
                  <Layout grow={1}>
                    <Divider vertical />
                  </Layout>
                </Row>
              </Layout>
              <Layout basis='35px' />
              <Layout grow={1}>
                <Row>
                  <Layout>
                    <Text size='small'>
                      Тариф
                    </Text>
                  </Layout>
                  <Layout basis='20px' />
                  <Layout>
                    <Row>
                      {activations.map((activation, index) => (
                        <Activation
                          key={index}
                          {...activation}
                          onChange={onChangeActivation}
                        />
                      ))}
                    </Row>
                  </Layout>
                </Row>
              </Layout>
              <Layout basis='35px' />
            </Column>
          </Layout>
          <Layout basis='30px' />
          <Layout>
            <Divider />
          </Layout>
          <Layout basis='15px' />
          <Layout>
            <Column align='center'>
              <Layout grow={1} />
              <Layout>
                <GhostButton
                  color='blue'
                  size='small'
                  onClick={onLoad}
                >
                  Показать
                </GhostButton>
              </Layout>
              <Layout basis='20px' />
            </Column>
          </Layout>
          <Layout basis='15px' />
        </Row>
      </div>
      <ContentDivider show={show} />
    </Container>
  </div>
)

export default Filters
