import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    position: 'absolute',
    zIndex: 2,
    width: '100%',
    top: '-400px',
    transition: 'all 0.5s ease-in-out',
    background: 'transparent',
  },
  show: {
    top: '0px',
  },
})

const Container = ({ children, show }) => (
  <div className={styles({ show })}>
    {children}
  </div>
)

export default Container
