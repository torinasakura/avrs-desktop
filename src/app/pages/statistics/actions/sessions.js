import gql from 'graphql-tag'
import * as actions from '../constants/sessions'

export function load() {
  return async (dispatch, getState, client) => {
    const filters = getState().statistics.filters

    const { from, to } = filters.period
    const activations = filters.activations
                               .filter(({ selected }) => selected)
                               .map(({ id }) => id)

    const { data } = await client.query({
      forceFetch: true,
      query: gql`
        query sessions ($from: String, $to: String, $activations: [ID!]) {
          sessions (from: $from, to: $to, activations: $activations) {
            id
            time
            startAt
            activation {
              id
              servicePlan {
                type
              }
            }
          }
        }
      `,
      variables: {
        from,
        to,
        activations,
      },
    })

    dispatch({
      type: actions.load,
      sessions: data.sessions,
      activations,
      from,
      to,
    })
  }
}
