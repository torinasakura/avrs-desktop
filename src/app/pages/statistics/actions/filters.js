import * as actions from '../constants/filters'
import { load as loadSessions } from './sessions'

export function toggle() {
  return { type: actions.toggle }
}

export function changePeriod(value) {
  return {
    type: actions.changePeriod,
    value,
  }
}

export function changeActivation(id, value) {
  return {
    type: actions.changeActivation,
    id,
    value,
  }
}

export function load() {
  return async (dispatch) => {
    dispatch(loadSessions())
    dispatch(toggle())
  }
}
