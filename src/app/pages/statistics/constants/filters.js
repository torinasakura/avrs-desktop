export const sync = '@@avrs-desktop/statistics/filters/SYNC'
export const toggle = '@@avrs-desktop/statistics/filters/TOGGLE'
export const changePeriod = '@@avrs-desktop/statistics/filters/CHANGE_PERIOD'
export const changeActivation = '@@avrs-desktop/statistics/filters/CHANGE_ACTIVATION'
