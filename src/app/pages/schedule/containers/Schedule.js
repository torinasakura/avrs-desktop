import { connect } from 'react-redux'
import { change, reset, save, changeActive } from '../actions'
import Schedule from '../components/Schedule'

export default connect(
  (state) => {
    const types = state.user.activations.reduce((result, activation) => ({
      ...result,
      [activation.id]: activation.servicePlan.type,
    }), {})

    return {
      previous: state.schedule.previous,
      schedules: state.schedule.schedules.map(schedule => ({
        ...schedule,
        type: types[schedule.id],
      })),
    }
  },
  dispatch => ({
    onChangeActive: id => dispatch(changeActive(id)),
    onChange: value => dispatch(change(value)),
    onReset: () => dispatch(reset()),
    onSave: () => dispatch(save()),
  }),
)(Schedule)
