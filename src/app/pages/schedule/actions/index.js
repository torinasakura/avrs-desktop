import { send } from '../../../actions/remote'
import * as actions from '../constants'

export function changeActive(id) {
  return {
    type: actions.changeActive,
    id,
  }
}

export function change(value) {
  return {
    type: actions.change,
    value,
  }
}

export function reset() {
  return {
    type: actions.reset,
  }
}

export function save() {
  return async (dispatch, getState) => {
    const schedules = getState().schedule.schedules.reduce((result, schedule) => ({
      ...result,
      [schedule.id]: {
        schedule: schedule.values,
      },
    }), {})

    dispatch(send('schedule:save', schedules))
  }
}

export function saved() {
  return {
    type: actions.saved,
  }
}
