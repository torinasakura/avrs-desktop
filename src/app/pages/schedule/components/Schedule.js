import React from 'react'
import { StyleSheet } from 'elementum'
import { Column, Row, Layout } from 'flex-layouts'
import { Schedule as ScheduleComponent } from 'avrs-ui/src/schedule'
import { GhostButton } from 'avrs-ui/src/button'
import { AutoSizer } from 'avrs-ui/src/content'
import { Text } from 'avrs-ui/src/text'
import Activaiton from './activation/Activation'

const styles = StyleSheet.create({
  self: {
    position: 'absolute',
    marginTop: '0px',
    height: '100%',
    top: '110%',
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 3,
    background: '#ffffff',
    boxSizing: 'border-box',
    transition: 'all 0.5s ease-in-out',
    borderTop: '1px solid #E1E4E6',
  },
  show: {
    top: '-1px',
  },
})

const Schedule = ({ show, schedules, previous, onChange, onSave, onReset, onChangeActive }) => (
  <div className={styles({ show })}>
    <Column fill>
      <Layout basis='45px' />
      <Layout shrink={1} basis='100%'>
        <Row>
          <Layout basis='25px' />
          <Layout>
            <Column align='center'>
              <Layout>
                <Text size='large' weight='light'>
                  Расписание
                </Text>
              </Layout>
              <Layout grow={1} />
              {schedules.map((schedule, index) => (
                <Layout key={index}>
                  <Activaiton
                    {...schedule}
                    onClick={() => onChangeActive(schedule.id)}
                  />
                </Layout>
              ))}
            </Column>
          </Layout>
          <Layout basis='15px' />
          <Layout shrink={1} basis='100%'>
            <AutoSizer>
              <ScheduleComponent
                values={schedules}
                onChange={onChange}
              />
            </AutoSizer>
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Column>
              <Layout basis='48%' />
              <Layout grow={1} />
              <Layout>
                <GhostButton color='blue' size='small' onClick={onSave} disabled={!previous}>
                  Сохранить
                </GhostButton>
              </Layout>
              <Layout basis='15px' />
              <Layout>
                <GhostButton size='small' onClick={onReset} disabled={!previous}>
                  Сбросить
                </GhostButton>
              </Layout>
            </Column>
          </Layout>
          <Layout basis='25px' />
        </Row>
      </Layout>
      <Layout basis='45px' />
    </Column>
  </div>
)

export default Schedule
