import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'inline-block',
    width: '9px',
    height: '9px',
    borderRadius: '50%',
    marginRight: '6px',
    opacity: 0.5,
  },
  active: {
    opacity: 1,
  },
})

const Color = ({ color, active }) => (
  <span
    style={{ background: color }}
    className={styles({ active })}
  />
)

export default Color
