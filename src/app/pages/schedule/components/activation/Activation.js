import React from 'react'
import { StyleSheet } from 'elementum'
import Color from './Color'

const names = {
  basis: 'Базис',
  standart: 'Стандарт',
  premium: 'Премиум',
  business: 'Бизнес',
}

const styles = StyleSheet.create({
  self: {
    fontSize: '14px',
    fontWeight: 300,
    fontFamily: '"Ubuntu", sans-serif',
    lineHeight: '12px',
    cursor: 'pointer',
    padding: '7px 20px',
    background: 'transparent',
    textDecoration: 'none',
    outline: 0,
    color: 'black',
    opacity: 0.7,
    '&:hover': {
      opacity: 0.9,
      '& span': {
        opacity: 0.9,
      },
    },
    '& span': {
      fontSize: '12px',
    },
  },
  active: {
    opacity: 1,
  },
})

const Activation = ({ id, type, active, color, onClick }) => (
  <div
    className={styles({ active })}
    onClick={onClick}
  >
    <Color
      color={color}
      active={active}
    />
    {names[type]} <span>(#{id})</span>
  </div>
)

export default Activation
