import { createReducer } from '../../../utils'
import * as actions from '../constants'

const colors = ['#00BB27', '#0288D1']

const initialState = {
  previous: null,
  schedules: [],
}

export default createReducer(initialState, {
  [actions.sync]: (state, { schedules }) => ({
    ...state,
    schedules: Object.keys(schedules).map((id, index) => ({
      id,
      active: index === 0,
      color: colors[index],
      values: schedules[id].schedule,
    })),
  }),
  [actions.change]: (state, { value }) => ({
    ...state,
    changed: true,
    schedules: value,
    previous: state.previous ? state.previous : state.schedules,
  }),
  [actions.reset]: state => ({ ...state, schedules: state.previous, previous: null }),
  [actions.changeActive]: (state, { id }) => ({
    ...state,
    schedules: state.schedules.map(schedule => ({ ...schedule, active: id === schedule.id })),
    previous: state.previous ? state.previous.map(schedule => ({ ...schedule, active: id === schedule.id })) : null,
  }),
  [actions.saved]: state => ({ ...state, previous: null }),
})
