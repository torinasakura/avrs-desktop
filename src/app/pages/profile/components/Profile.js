import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Text, Space } from 'avrs-ui/src/text'
import { LogOutIcon } from 'avrs-ui/src/icons'
import { Link } from 'avrs-ui/src/link'

const Profile = ({ firstName, lastName, onLogout }) => (
  <Column>
    <Layout basis='45px' />
    <Layout grow={1}>
      <Row>
        <Layout basis='30px' />
        <Layout>
          <Column align='center'>
            <Layout>
              <Text size='xlarge' weight='light'>
                {firstName}
                <Space />
                {lastName}
              </Text>
            </Layout>
            <Layout grow={1} />
            <Layout align='center'>
              <LogOutIcon fill='#0288d1' />
              <Space />
              <Link onClick={onLogout}>
                <Text color='blue400' weight='light' lineHeight='large'>
                  Выйти
                </Text>
              </Link>
            </Layout>
            <Layout basis='10px' />
          </Column>
        </Layout>
        <Layout basis='20px' />
      </Row>
    </Layout>
    <Layout basis='45px' />
  </Column>
)

export default Profile
