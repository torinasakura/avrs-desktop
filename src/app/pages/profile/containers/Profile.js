import { connect } from 'react-redux'
import { send } from '../../../actions/remote'
import Profile from '../components/Profile'

export default connect(
  state => ({
    firstName: state.user.firstName,
    lastName: state.user.lastName,
  }),
  dispatch => ({
    onLogout: () => dispatch(send('auth:logout')),
  }),
)(Profile)
