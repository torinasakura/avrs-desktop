import Profile from '../containers/Profile'

export default function getRoutes() {
  return {
    path: 'profile',
    component: Profile,
  }
}
