import gql from 'graphql-tag'
import { shell } from 'electron'

export function open() {
  return async (dispatch, getState, client) => {
    const { type, period } = getState().servicePlans

    const { data } = await client.query({
      forceFetch: true,
      query: gql`
        query {
          generateShopLink (type: "${type}", period: "${period}") {
            url
          }
        }
      `,
    })

    if (data.generateShopLink.url) {
      shell.openExternal(data.generateShopLink.url)
    }
  }
}
