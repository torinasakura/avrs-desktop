import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Text, Space } from 'avrs-ui/src/text'
import { Divider } from 'avrs-ui/src/divider'
import { Slider } from 'avrs-ui/src/slider'
import { Link } from 'avrs-ui/src/link'

const names = {
  basis: 'Базис',
  standart: 'Стандарт',
  premium: 'Премиум',
  business: 'Бизнес',
}

const Beginning = ({ plan = {}, onChangePeriod, onChangeTime, onChangeCPU, onChangeMemory, onOpen }) => (
  <Row>
    <Layout grow={1} />
    <Layout>
      <Column>
        <Layout grow={1} />
        <Layout basis='25px' />
        <Layout basis='310px'>
          <Row>
            <Layout basis='70px'>
              <Text
                weight='light'
                color='gray250'
                lineHeight='extended'
              >
                Настройте параметры и мы подскажем, какой тариф вам подходит:
              </Text>
            </Layout>
            <Layout>
              <Text
                size='small'
                weight='light'
                color='gray250'
              >
                Сколько ваш ПК может находиться в сети?
              </Text>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Slider
                prefix='ч.'
                value={plan.time}
                markers={[10, 12, 15, 20]}
                onChange={onChangeTime}
              />
            </Layout>
            <Layout basis='45px' />
            <Layout>
              <Text
                size='small'
                weight='light'
                color='gray250'
              >
                Планируемый арендный ресурс CPU
              </Text>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Slider
                prefix='%'
                value={plan.cpu}
                markers={[5, 15, 20, 40]}
                onChange={onChangeCPU}
              />
            </Layout>
            <Layout basis='45px' />
            <Layout>
              <Text
                size='small'
                weight='light'
                color='gray250'
              >
                Планируемый арендный ресурс RAM Gb
              </Text>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Slider
                prefix='Gb'
                value={plan.memory / 1024}
                markers={[1, 2, 3, 4]}
                onChange={onChangeMemory}
              />
            </Layout>
          </Row>
        </Layout>
        <Layout basis='80px' />
        <Layout>
          <Row fill>
            <Layout basis='70px' />
            <Layout grow={1}>
              <Divider vertical />
            </Layout>
          </Row>
        </Layout>
        <Layout basis='45px' />
        <Layout basis='300px'>
          <Row>
            <Layout basis='130px' />
            <Layout>
              <Text
                weight='light'
                color='gray250'
                lineHeight='extended'
              >
                Ваш заработок:
              </Text>
            </Layout>
            <Layout basis='20px' />
            <Layout>
              <Text size='large'>
                {plan.amount} €
              </Text>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Text
                color='gray250'
                weight='light'
                size='small'
              >
                С тарифом
              </Text>
              <Space />
              <Text
                color='blue400'
                weight='light'
                size='small'
              >
                {names[plan.type]}
              </Text>
              <Space />
              <Text
                color='gray250'
                weight='light'
                size='small'
              >
                за
              </Text>
              <Space />
              <Text
                color='blue400'
                weight='light'
                size='small'
              >
                {plan.period} дней
              </Text>
              <Space />
              <Text
                color='gray250'
                weight='light'
                size='small'
              >
                подписки
              </Text>
            </Layout>
            <Layout basis='25px' />
            <Layout>
              <Slider
                value={plan.period}
                markers={[30, 90, 180, 365]}
                onChange={onChangePeriod}
              />
            </Layout>
          </Row>
        </Layout>
        <Layout grow={1} />
      </Column>
    </Layout>
    <Layout grow={1} />
    <Layout>
      <Divider />
    </Layout>
    <Layout basis='15px' />
    <Layout>
      <Column>
        <Layout grow={1} />
        <Layout>
          <Link onClick={onOpen}>
            <Text
              color='gray250'
              size='small'
            >
              Купить
            </Text>
          </Link>
          <Space />
          <Text
            color='gray250'
            size='small'
          >
            &#10095;
          </Text>
        </Layout>
        <Layout basis='25px' />
      </Column>
    </Layout>
    <Layout basis='15px' />
  </Row>
)

export default Beginning
