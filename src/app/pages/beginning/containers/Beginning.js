import { connect } from 'react-redux'
import { open } from '../actions'
import { changePeriod, changeTime, changeCPU, changeMemory } from '../../ServicePlans/actions'
import Beginning from '../components/Beginning'

export default connect(
  state => ({
    plan: state.servicePlans.active || {},
  }),
  dispatch => ({
    onOpen: () => dispatch(open()),
    onChangePeriod: period => dispatch(changePeriod(period)),
    onChangeTime: time => dispatch(changeTime(time)),
    onChangeCPU: cpu => dispatch(changeCPU(cpu)),
    onChangeMemory: memory => dispatch(changeMemory(memory)),
  }),
)(Beginning)
