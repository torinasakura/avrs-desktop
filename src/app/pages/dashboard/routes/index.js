import Dashboard from '../components/Dashboard'

export default function getRoutes() {
  return {
    path: 'dashboard',
    component: Dashboard,
  }
}
