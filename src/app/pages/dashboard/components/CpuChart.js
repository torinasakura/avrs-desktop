import React, { Component } from 'react'
import { PieLiveChart } from 'avrs-ui/src/chart'
import { ipcRenderer } from 'electron'

class CpuChart extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      value: 0,
    }
  }

  componentDidMount() {
    ipcRenderer.on('session:stat', this.onStat)
  }

  componentWillUnmount() {
    ipcRenderer.removeListener('session:stat', this.onStat)
  }

  onStat = (event, data) => {
    this.setState({ value: data.c / 100 })
  }

  render() {
    const { value } = this.state

    return (
      <PieLiveChart
        width={200}
        height={200}
        value={value}
      />
    )
  }
}

export default CpuChart
