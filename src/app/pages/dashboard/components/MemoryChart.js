import React, { Component } from 'react'
import { LineLiveChart } from 'avrs-ui/src/chart'
import { AutoSizer } from 'avrs-ui/src/content'
import { ipcRenderer } from 'electron'

class MemoryChart extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      value: null,
    }
  }

  componentDidMount() {
    ipcRenderer.on('session:stat', this.onStat)
  }

  componentWillUnmount() {
    ipcRenderer.removeListener('session:stat', this.onStat)
  }

  onStat = (event, data) => {
    this.setState({ value: data.m })
  }

  render() {
    const { value } = this.state

    return (
      <AutoSizer>
        <LineLiveChart
          value={value}
        />
      </AutoSizer>
    )
  }
}

export default MemoryChart
