import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Text } from 'avrs-ui/src/text'
import MemoryChart from './MemoryChart'
import CpuChart from './CpuChart'

const Dashboard = () => (
  <Row>
    <Layout grow={1}>
      <Column>
        <Layout basis='45px' />
        <Layout grow={1}>
          <Row>
            <Layout basis='35px' />
            <Layout grow={1}>
              <Column>
                <Layout shrink={1} basis='100%'>
                  <Row>
                    <Layout>
                      <Text size='large' weight='light'>
                        Использование памяти
                      </Text>
                    </Layout>
                    <Layout basis='35px' />
                    <Layout grow={1}>
                      <MemoryChart />
                    </Layout>
                  </Row>
                </Layout>
                <Layout basis='45px' />
                <Layout basis='240px'>
                  <Row>
                    <Layout>
                      <Text size='large' weight='light'>
                        Загрузка процессора
                      </Text>
                    </Layout>
                    <Layout grow={1} />
                    <Layout basis='200px' justify='center'>
                      <CpuChart />
                    </Layout>
                    <Layout grow={1} />
                  </Row>
                </Layout>
              </Column>
            </Layout>
            <Layout basis='45px' />
          </Row>
        </Layout>
        <Layout basis='40px' />
      </Column>
    </Layout>
  </Row>
)

export default Dashboard
