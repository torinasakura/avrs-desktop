import gql from 'graphql-tag'
import { shell } from 'electron'
import { send } from '../../../actions/remote'
import * as actions from '../constants/login'

export function change(field, value) {
  return {
    type: actions.change,
    field,
    value,
  }
}

export function login() {
  return async (dispatch, getState, client) => {
    const { email, password } = getState().auth.login

    const { data } = await client.mutate({
      mutation: gql`
        mutation loginUser($email: String!, $password: String!) {
          loginUser(email: $email, password: $password) {
            token { id, email, token }
            errors {
              key
              message
            }
          }
        }
      `,
      variables: {
        email,
        password,
      },
    })

    if (data.loginUser.errors.length > 0) {
      dispatch({
        type: actions.setErrors,
        errors: data.loginUser.errors,
      })
    } else {
      dispatch(send('auth:login', data.loginUser.token))
    }
  }
}

export function openRegistration() {
  return async () => {
    shell.openExternal(`${process.env.CABINET_URL}#/auth/registration`)
  }
}

export function openResetPassword() {
  return async () => {
    shell.openExternal(`${process.env.CABINET_URL}#/auth/reset_password`)
  }
}
