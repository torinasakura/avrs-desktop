import React, { PropTypes } from 'react'
import { StyleSheet } from 'elementum'
import { Row, Layout } from 'flex-layouts'
import { LogoWithText } from 'avrs-ui/src/logo'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    height: '100%',
    background: '#ffffff',
  },
})

const Container = ({ children }) => (
  <div className={styles()}>
    <Row fill>
      <Layout basis='50px' />
      <Layout justify='center'>
        <LogoWithText height={30} />
      </Layout>
      <Layout basis='20px' />
      <Layout grow={1}>
        {children}
      </Layout>
    </Row>
  </div>
)

Container.propTypes = {
  children: PropTypes.element,
}

export default Container
