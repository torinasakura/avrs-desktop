import React, { PropTypes } from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Text, Space } from 'avrs-ui/src/text'
import { Input } from 'avrs-ui/src/input'
import { Link } from 'avrs-ui/src/link'
import { Button } from 'avrs-ui/src/button'
import Container from './Container'
import Info from './Info'

const Login = ({
  email, password, errors, onChangeEmail, onChangePassword,
  onLogin, onOpenRegistration, onOpenResetPassword,
}) => (
  <Container>
    <Row>
      <Layout>
        <Column>
          <Layout basis='35px' />
          <Layout shrink={1} basis='100%'>
            <Row>
              <Layout basis='30px' />
              <Layout justify='center'>
                <Text size='large'>
                  Вход
                </Text>
              </Layout>
              <Layout basis='4px' />
              <Layout basis='25px' />
              <Layout>
                <Input
                  value={email}
                  placeholder='Email адрес'
                  onChange={onChangeEmail}
                />
              </Layout>
              <Layout basis='15px' />
              <Layout>
                <Input
                  type='password'
                  value={password}
                  placeholder='Пароль'
                  onChange={onChangePassword}
                />
              </Layout>
              <Layout basis='8px' />
              <Layout justify='center'>
                <Text color='red400' size='xsmall'>
                  {errors.email || <Space />}
                </Text>
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <Column>
                  <Layout align='center'>
                    <Link onClick={onOpenResetPassword}>
                      <Text size='xsmall' color='blue400'>
                        Забыли пароль?
                      </Text>
                    </Link>
                  </Layout>
                  <Layout grow={1} />
                  <Layout>
                    <Button onClick={onLogin}>
                      Войти
                    </Button>
                  </Layout>
                </Column>
              </Layout>
              <Layout basis='30px' />
            </Row>
          </Layout>
          <Layout basis='35px' />
        </Column>
      </Layout>
      <Layout grow={1} />
      <Layout>
        <Info>
          <Text size='xsmall' lineHeight='extended'>
            Впервые на Aversis?
          </Text>
          <Space />
          <Link onClick={onOpenRegistration}>
            <Text color='blue400' size='xsmall' lineHeight='extended'>
              Стать участником
            </Text>
          </Link>
        </Info>
      </Layout>
    </Row>
  </Container>
)

Login.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  onChangeEmail: PropTypes.func,
  onChangePassword: PropTypes.func,
  onLogin: PropTypes.func,
}

export default Login
