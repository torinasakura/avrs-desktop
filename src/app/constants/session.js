export const started = '@@avrs-desktop/session/STARTED'
export const stopped = '@@avrs-desktop/session/STOPPED'
export const wait = '@@avrs-desktop/session/WAIT'
