/* eslint-disable global-require */
if (process.env.NODE_ENV === 'production') {
  module.exports = require('./main/configureStore.prod')
} else {
  module.exports = require('./main/configureStore.dev')
}
