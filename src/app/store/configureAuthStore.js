/* eslint-disable global-require */
if (process.env.NODE_ENV === 'production') {
  module.exports = require('./auth/configureStore.prod')
} else {
  module.exports = require('./auth/configureStore.dev')
}
