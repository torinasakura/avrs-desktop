import { createStore, compose, applyMiddleware } from 'redux'
import { reduxReactRouter } from 'redux-router'
import { createHashHistory as createHistory } from 'history'
import api from '../middleware/api'
import rootReducer from '../../reducers/auth'
import client from '../../api/client'

const enhancer = compose(
  reduxReactRouter({ createHistory }),
  applyMiddleware(api(client)),
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  return store
}
