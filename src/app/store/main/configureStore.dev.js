import { createStore, compose, applyMiddleware } from 'redux'
import { reduxReactRouter } from 'redux-router'
import { createHashHistory as createHistory } from 'history'
import api from '../middleware/api'
import rootReducer from '../../reducers/main'
import client from '../../api/client'

const enhancer = compose(
  reduxReactRouter({ createHistory }),
  applyMiddleware(client.middleware(), api(client)),
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  if (module.hot) {
    module.hot.accept('../../reducers/main', () => store.replaceReducer(rootReducer))
  }

  return store
}
