import React from 'react'
import { render } from 'react-dom'
import 'flex-layouts/lib/flex-layouts.css'
import 'reset.css'
import configureStore from './store/configureAuthStore'
import Root from './containers/Root'
import './index.css'

const store = configureStore(JSON.parse(process.env.INITIAL_STATE || '{}'))

render(
  <Root store={store} />,
  document.getElementById('container'),
)

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    render(
      <Root store={store} />,
      document.getElementById('container'),
    )
  })
}
