/* eslint-disable no-param-reassign */
import ApolloClient, { createNetworkInterface } from 'apollo-client'
import { ipcRenderer } from 'electron'

const networkInterface = createNetworkInterface({ uri: process.env.API_URL })

const client = new ApolloClient({
  networkInterface,
})

networkInterface.use([{
  applyMiddleware(req, next) {
    const security = client.store.getState().security

    if (!req.options.headers) {
      req.options.headers = {}
    }

    if (security && security.token) {
      req.options.headers.authorization = security.token
    }

    next()
  },
}])

networkInterface.useAfter([{
  applyAfterware({ response }, next) {
    if (response.status === 401) {
      ipcRenderer.send('auth:logout')
    }

    next()
  },
}])

export default client
