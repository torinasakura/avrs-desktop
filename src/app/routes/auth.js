import auth from '../pages/auth/routes'

export default function getRoutes() {
  return {
    path: '/',
    indexRoute: auth(),
  }
}
