import App from '../containers/App'
import init from '../pages/init/routes'
import beginning from '../pages/beginning/routes'
import dashboard from '../pages/dashboard/routes'
import profile from '../pages/profile/routes'
import statistics from '../pages/statistics/routes'

export default function getRoutes(store) {
  return {
    path: '/',
    indexRoute: init(),
    childRoutes: [{
      component: App,
      childRoutes: [
        beginning(store),
        dashboard(store),
        statistics(store),
        profile(store),
      ],
    }],
  }
}
