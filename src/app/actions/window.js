import { remote } from 'electron'

export function minimize() {
  return async () => remote.BrowserWindow.getFocusedWindow().minimize()
}

export function maximize() {
  return async () => {
    const win = remote.BrowserWindow.getFocusedWindow()

    if (win.isMaximized()) {
      win.unmaximize()
    } else {
      win.maximize()
    }
  }
}

export function close() {
  return async () => remote.BrowserWindow.getFocusedWindow().close()
}
