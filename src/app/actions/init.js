import { sync as syncUser } from '../constants/user'
import { sync as syncToken } from '../constants/security'
import { sync as syncServicePlans } from '../pages/ServicePlans/constants'
import { sync as syncSchedule } from '../pages/schedule/constants'
import { sync as syncFilters } from '../pages/statistics/constants/filters'

export function init(data) {
  return async (dispatch, getState) => {
    const { user, token, servicePlans, schedules } = JSON.parse(data)

    dispatch({
      type: syncToken,
      token,
    })

    dispatch({
      type: syncUser,
      user,
    })

    dispatch({
      type: syncServicePlans,
      servicePlans,
    })

    dispatch({
      type: syncSchedule,
      schedules,
    })

    dispatch({
      type: syncFilters,
      activations: user.activations,
    })

    setTimeout(() => {
      if (getState().user.isNew) {
        window.location.hash = '/beginning'
      } else {
        window.location.hash = '/dashboard'
      }
    }, 1500)
  }
}
