import { ipcRenderer } from 'electron'

export function send(event, ...args) {
  return async () => ipcRenderer.send(event, ...args)
}
