import * as actions from '../constants/session'
import { send } from './remote'

export function started(data) {
  return async (dispatch, getState) => {
    const [activation] = getState().user.activations.filter(item => item.id === data.activation)

    dispatch({
      type: actions.started,
      data: {
        ...data,
        activation,
      },
    })
  }
}

export function stopped() {
  return {
    type: actions.stopped,
  }
}

export function wait(data) {
  return async (dispatch, getState) => {
    const [activation] = getState().user.activations.filter(item => item.id === data.activation)

    dispatch({
      type: actions.wait,
      data: {
        ...data,
        activation,
      },
    })
  }
}

export function start() {
  return send('session:start')
}

export function stop() {
  return send('session:stop')
}
