import React from 'react'
import { render } from 'react-dom'
import moment from 'moment'
import { ipcRenderer } from 'electron'
import 'flex-layouts/lib/flex-layouts.css'
import 'reset.css'
import configureStore from './store/configureMainStore'
import Root from './containers/Root'
import { init } from './actions/init'
import { started, stopped, wait } from './actions/session'
import { saved } from './pages/schedule/actions'
import './index.css'

moment.locale('ru')

const store = configureStore()

ipcRenderer.on('init', (event, data) => store.dispatch(init(data)))
ipcRenderer.on('location:change', (event, location) => store.history.push(`/${location}`))
ipcRenderer.on('session:started', (event, data) => store.dispatch(started(data)))
ipcRenderer.on('session:stopped', (event, data) => store.dispatch(stopped(data)))
ipcRenderer.on('session:wait', (event, data) => store.dispatch(wait(data)))
ipcRenderer.on('schedule:saved', (event, data) => store.dispatch(saved(data)))

render(
  <Root main store={store} />,
  document.getElementById('container'),
)

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    render(
      <Root store={store} />,
      document.getElementById('container'),
    )
  })
}
