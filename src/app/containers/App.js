import React from 'react'
import { connect } from 'react-redux'
import { Row, Layout } from 'flex-layouts'
import { Condition } from 'avrs-ui/src/condition'
import Header from './Header'
import Controls from './Controls'
import Schedule from '../pages/schedule/containers/Schedule'

const App = ({ children, isNew, showSchedule }) => (
  <Row fill>
    <Layout>
      <Header />
    </Layout>
    <Layout shrink={1} grow={1}>
      <div style={{ position: 'relative', width: '100%' }}>
        <Row fill>
          <Layout grow={1}>
            {children}
          </Layout>
          <Layout>
            <Schedule show={showSchedule} />
          </Layout>
        </Row>
      </div>
    </Layout>
    <Condition match={!isNew}>
      <Layout>
        <Controls />
      </Layout>
    </Condition>
  </Row>
)

export default connect(
  (state, { router }) => ({
    isNew: state.user.isNew,
    showSchedule: router.getCurrentLocation().query.schedule,
  }),
)(App)
