import { connect } from 'react-redux'
import { minimize, maximize, close } from '../actions/window'
import Header from '../components/header/Header'

export default connect(
  state => ({
    isNew: state.user.isNew,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
  }),
  dispatch => ({
    onMinimize: () => dispatch(minimize()),
    onMaximize: () => dispatch(maximize()),
    onClose: () => dispatch(close()),
  }),
)(Header)
