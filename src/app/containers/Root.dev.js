import React from 'react'
import { Provider } from 'react-redux'
import storeShape from 'react-redux/lib/utils/storeShape'
import { AppContainer } from 'react-hot-loader'
import ReduxRouter from './ReduxRouter'
import getMainRoutes from '../routes/main'
import getAuthRoutes from '../routes/auth'

const Root = ({ store, main }) => (
  <AppContainer>
    <Provider store={store}>
      <ReduxRouter routes={main ? getMainRoutes(store) : getAuthRoutes(store)} />
    </Provider>
  </AppContainer>
)

Root.propTypes = {
  store: storeShape,
}

export default Root
