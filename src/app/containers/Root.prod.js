import React from 'react'
import { Provider } from 'react-redux'
import storeShape from 'react-redux/lib/utils/storeShape'
import ReduxRouter from './ReduxRouter'
import getMainRoutes from '../routes/main'
import getAuthRoutes from '../routes/auth'

const Root = ({ main, store }) => (
  <Provider store={store}>
    <ReduxRouter routes={main ? getMainRoutes(store) : getAuthRoutes(store)} />
  </Provider>
)

Root.propTypes = {
  store: storeShape,
}

export default Root
