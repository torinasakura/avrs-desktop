import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { start, stop } from '../actions/session'
import Controls from '../components/controls/Controls'

export default withRouter(connect(
  state => ({
    started: state.session.started,
    startAt: state.session.startAt,
    endAt: state.session.endAt,
    nextStartAt: state.session.nextStartAt,
    activation: state.session.activation,
  }),
  (dispatch, { router }) => ({
    onToggleShedule: () => {
      const location = router.getCurrentLocation()
      const { schedule, ...query } = location.query

      if (schedule) {
        router.replace({ ...location, query })
      } else {
        router.replace({ ...location, query: { ...query, schedule: true } })
      }
    },
    onStart: () => dispatch(start()),
    onStop: () => dispatch(stop()),
  }),
)(Controls))
