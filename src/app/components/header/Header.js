import React from 'react'
import { StyleSheet } from 'elementum'
import { Column, Row, Layout } from 'flex-layouts'
import { Condition } from 'avrs-ui/src/condition'
import { LogoWithText } from 'avrs-ui/src/logo'
import { Divider } from 'avrs-ui/src/divider'
import { NavLink } from 'avrs-ui/src/link'
import { AccountIcon } from 'avrs-ui/src/icons'
import { Controls } from './controls'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    display: 'flex',
    '-webkit-user-select': 'none',
    '-webkit-app-region': 'drag',
  },
})

const Header = ({ isNew, firstName, lastName, onMinimize, onMaximize, onClose }) => (
  <div className={styles()}>
    <Row>
      <Layout basis='56px'>
        <Column align='center'>
          <Layout basis='30px' />
          <Layout>
            <LogoWithText height={15} />
          </Layout>
          <Layout basis='40px' />
          <Layout shrink={1} basis='100%'>
            <Row>
              <Layout basis='3px' />
              <Layout>
                <Column align='center'>
                  <Condition match={isNew}>
                    <Layout>
                      <NavLink to='/beginning'>
                        Начало работы
                      </NavLink>
                    </Layout>
                  </Condition>
                  <Condition match={!isNew}>
                    <Layout>
                      <NavLink to='/dashboard'>
                        Сессия
                      </NavLink>
                    </Layout>
                  </Condition>
                  <Layout basis='30px' />
                  <Condition match={!isNew}>
                    <Layout>
                      <NavLink to='/statistics'>
                        Статистика
                      </NavLink>
                    </Layout>
                  </Condition>
                  <Layout grow={1} />
                  <Layout align='center'>
                    <span style={{ position: 'relative', width: 32, height: 26 }}>
                      <span style={{ position: 'absolute', top: 0 }}>
                        <AccountIcon height={24} />
                      </span>
                    </span>
                    <NavLink to='/profile'>
                      {firstName} {lastName}
                    </NavLink>
                  </Layout>
                </Column>
              </Layout>
            </Row>
          </Layout>
          <Layout basis='60px' />
          <Layout>
            <Controls
              onMinimize={onMinimize}
              onMaximize={onMaximize}
              onClose={onClose}
            />
          </Layout>
          <Layout basis='10px' />
        </Column>
      </Layout>
      <Layout>
        <Divider />
      </Layout>
    </Row>
  </div>
)

export default Header
