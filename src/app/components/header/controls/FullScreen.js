import React from 'react'
import Icon from 'avrs-ui/src/icons/Icon'
import Control from './Control'

const FullScreen = ({ onClick }) => (
  <Control onClick={onClick}>
    <Icon originalWidth={14} originalHeight={14}>
      <defs>
        <rect id='path-1' x='36' y='0' width='14' height='14' />
        <mask
          id='mask-2'
          maskContentUnits='userSpaceOnUse'
          maskUnits='objectBoundingBox'
          x='0' y='0' width='14' height='14' fill='white'
        >
          <use xlinkHref='#path-1' />
        </mask>
      </defs>
      <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
        <g transform='translate(-1039.000000, -284.000000)' stroke='#9B9B9B' strokeWidth='4'>
          <g transform='translate(115.000000, 265.000000)'>
            <g transform='translate(310.000000, 9.000000)'>
              <g transform='translate(578.000000, 10.000000)'>
                <use mask='url(#mask-2)' xlinkHref='#path-1' />
              </g>
            </g>
          </g>
        </g>
      </g>
    </Icon>
  </Control>
)

export default FullScreen
