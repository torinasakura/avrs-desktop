import React from 'react'
import Icon from 'avrs-ui/src/icons/Icon'
import Control from './Control'

const Hide = ({ onClick }) => (
  <Control
    align='bottom'
    onClick={onClick}
  >
    <Icon originalWidth={16} originalHeight={2}>
      <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
        <g transform='translate(-1003.000000, -296.000000)' fill='#9B9B9B'>
          <g transform='translate(115.000000, 265.000000)'>
            <g transform='translate(310.000000, 9.000000)'>
              <g transform='translate(578.000000, 10.000000)'>
                <rect id='Hide' x='0' y='12' width='16' height='2' />
              </g>
            </g>
          </g>
        </g>
      </g>
    </Icon>
  </Control>
)

export default Hide
