import React from 'react'
import Icon from 'avrs-ui/src/icons/Icon'
import Control from './Control'

const Close = ({ onClick }) => (
  <Control
    red
    onClick={onClick}
  >
    <Icon originalWidth={14} originalHeight={14}>
      <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
        <g className='control-element' transform='translate(-1071.000000, -284.000000)' fill='#9B9B9B'>
          <g transform='translate(115.000000, 265.000000)'>
            <g transform='translate(310.000000, 9.000000)'>
              <g transform='translate(578.000000, 10.000000)'>
                <polyline
                  points='82 12.5797101 80.5797101 14 75 8.42028986 69.4202899
                          14 68 12.5797101 73.5797101 7 68 1.42028986 69.4202899
                          0 75 5.57971014 80.5797101 0 82 1.42028986 76.4202899 7 82 12.5797101'
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </Icon>
  </Control>
)

export default Close
