import React from 'react'
import { StyleSheet } from 'elementum'
import Hide from './Hide'
import FullScreen from './FullScreen'
import Close from './Close'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '0px',
  },
})

const Controls = ({ onMinimize, onMaximize, onClose }) => (
  <div className={styles()}>
    <Hide onClick={onMinimize} />
    <FullScreen onClick={onMaximize} />
    <Close onClick={onClose} />
  </div>
)

export default Controls
