import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    padding: '9px 12px 8px 12px',
    display: 'flex',
    cursor: 'pointer',
    WebkitAppRegion: 'no-drag',
    '&:hover': {
      background: '#f2f2f2',
    },
  },
  red: {
    '&:hover': {
      background: '#ff0000',
      '& .control-element': {
        fill: '#ffffff',
      },
    },
  },
  'align=bottom': {
    paddingTop: '12px',
  },
})

const Control = ({ children, align, red, onClick }) => (
  <div
    className={styles({ align, red })}
    onClick={onClick}
  >
    {children}
  </div>
)

export default Control
