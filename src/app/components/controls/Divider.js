import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    width: '100%',
    height: '5px',
    '& div': {
      position: 'absolute',
      bottom: 0,
      height: '10px',
      width: '110%',
      boxShadow: 'inset 0 -1px 2px 0px rgba(0, 0, 0, 0.05)',
      borderBottom: '1px solid #E1E4E6',
    },
  },
})

const Divider = () => (
  <div className={styles()}>
    <div />
  </div>
)

export default Divider
