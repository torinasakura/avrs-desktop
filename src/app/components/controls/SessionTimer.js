import React, { Component } from 'react'
import moment from 'moment'
import { Column, Row, Layout } from 'flex-layouts'
import { Text } from 'avrs-ui/src/text'
import { ipcRenderer } from 'electron'

const SessionTimer = ({ currentTime, endTime }) => (
  <Column align='center'>
    <Layout>
      <Row>
        <Layout>
          <Text
            size='large'
            weight='light'
          >
            {moment.utc(currentTime).format('HH:mm:ss')}
          </Text>
        </Layout>
        <Layout>
          <Text
            size='xsmall'
            weight='light'
            color='gray400'
          >
            Текущее время
          </Text>
        </Layout>
      </Row>
    </Layout>
    <Layout>
      <div style={{ padding: '0 10px', fontSize: 18 }}>
        /
      </div>
    </Layout>
    <Layout>
      <Row>
        <Layout>
          <Text
            size='large'
            weight='light'
          >
            {moment.utc(endTime).format('HH:mm:ss')}
          </Text>
        </Layout>
        <Layout>
          <Text
            size='xsmall'
            weight='light'
            color='gray400'
          >
            Осталось
          </Text>
        </Layout>
      </Row>
    </Layout>
  </Column>
)

class SessionTimerContainer extends Component {
  componentDidMount() {
    ipcRenderer.on('session:tick', this.onTick)
  }

  componentWillUnmount() {
    ipcRenderer.removeListener('session:tick', this.onTick)
  }

  onTick = () => {
    this.forceUpdate()
  }

  render() {
    const { startAt, endAt } = this.props

    const currentTime = startAt ? (new Date()).getTime() - startAt : 0
    const endTime = endAt ? endAt - (new Date()).getTime() : 0

    return (
      <SessionTimer
        currentTime={currentTime}
        endTime={endTime}
      />
    )
  }
}

export default SessionTimerContainer
