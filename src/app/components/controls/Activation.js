import React from 'react'
import humanizeDuration from 'humanize-duration'
import { Row, Layout } from 'flex-layouts'
import { Text, Space } from 'avrs-ui/src/text'

const names = {
  basis: 'Базис',
  standart: 'Стандарт',
  premium: 'Премиум',
  business: 'Бизнес',
}

const Activation = ({ leftTime, servicePlan = {} }) => (
  <Row>
    <Layout justify='end'>
      <Text weight='light'>
        Тариф
      </Text>
      <Space />
      <Text>
        {names[servicePlan.type]}
      </Text>
    </Layout>
    <Layout basis='2px' />
    <Layout justify='end'>
      <Text size='xsmall' weight='light' color='gray400'>
        Осталось {humanizeDuration(leftTime * 1000, { language: 'ru', units: ['d'], round: true })}
      </Text>
    </Layout>
  </Row>
)

export default Activation
