import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Condition } from 'avrs-ui/src/condition'
import { GhostButton } from 'avrs-ui/src/button'
import { Text, Space } from 'avrs-ui/src/text'
import Icon from 'avrs-ui/src/icons/Icon'
import SessionStopped from './SessionStopped'
import SessionTimer from './SessionTimer'
import SessionWait from './SessionWait'
import Activation from './Activation'
import Divider from './Divider'

const ScheduleIcon = props => (
  <Icon originalWidth={24} originalHeight={24} {...props}>
    <g>
      <path
        d='M12.516 6.984v5.25l4.5 2.672-0.75 1.266-5.25-3.188v-6h1.5zM12
          20.016c4.406 0 8.016-3.609 8.016-8.016s-3.609-8.016-8.016-8.016-8.016
          3.609-8.016 8.016 3.609 8.016 8.016 8.016zM12 2.016c5.531 0 9.984 4.453
          9.984 9.984s-4.453 9.984-9.984 9.984-9.984-4.453-9.984-9.984 4.453-9.984 9.984-9.984z'
      />
    </g>
  </Icon>
)

const Controls = ({ started, startAt, endAt, nextStartAt, activation, onToggleShedule, onStart, onStop }) => (
  <Row>
    <Layout>
      <Divider />
    </Layout>
    <Layout basis='15px' />
    <Layout basis='42px'>
      <Column align='center'>
        <Layout basis='45px' />
        <Layout basis='235px'>
          <Condition match={started && startAt}>
            <SessionTimer
              startAt={startAt}
              endAt={endAt}
            />
          </Condition>
          <Condition match={started && nextStartAt}>
            <SessionWait nextStartAt={nextStartAt} />
          </Condition>
          <Condition match={!started}>
            <SessionStopped />
          </Condition>
        </Layout>
        <Layout>
          <div
            style={{ cursor: 'pointer', marginTop: 6 }}
            onClick={onToggleShedule}
          >
            <ScheduleIcon
              height={18}
              fill='#0288d1'
            />
            <Space />
            <span style={{ position: 'relative', top: -4 }}>
              <Text size='xsmall' color='blue400'>
                Расписание
              </Text>
            </span>
          </div>
        </Layout>
        <Layout grow={1} />
        <Layout>
          <Condition match={activation}>
            <Activation {...activation} />
          </Condition>
        </Layout>
        <Layout basis='40px' />
        <Layout>
          <Condition match={started}>
            <GhostButton color='blue' size='small' onClick={onStop}>
              Остановить
            </GhostButton>
          </Condition>
          <Condition match={!started}>
            <GhostButton color='blue' size='small' onClick={onStart}>
              Запустить
            </GhostButton>
          </Condition>
        </Layout>
        <Layout basis='45px' />
      </Column>
    </Layout>
    <Layout basis='15px' />
  </Row>
)

export default Controls
