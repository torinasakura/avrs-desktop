import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Text } from 'avrs-ui/src/text'

const SessionStopped = () => (
  <Column align='center'>
    <Layout>
      <Row>
        <Layout>
          <Text
            size='medium'
            weight='light'
          >
            Сессия не запущена
          </Text>
        </Layout>
      </Row>
    </Layout>
  </Column>
)

export default SessionStopped
