import React, { Component } from 'react'
import moment from 'moment'
import { Column, Row, Layout } from 'flex-layouts'
import { Text } from 'avrs-ui/src/text'
import { ipcRenderer } from 'electron'

class SessionWait extends Component {
  componentDidMount() {
    this.onTick()
    ipcRenderer.on('session:tick', this.onTick)
  }

  componentWillUnmount() {
    ipcRenderer.removeListener('session:tick', this.onTick)
  }

  onTick = () => {
    if (this.timer) {
      const { nextStartAt } = this.props

      const startTime = nextStartAt ? nextStartAt - (new Date()).getTime() : 0

      this.timer.textContent = moment.utc(startTime).format('HH:mm:ss')
    }
  }

  onSetRef = (element) => {
    this.timer = element
  }

  render() {
    return (
      <Column align='center'>
        <Layout>
          <Row>
            <Layout>
              <Text
                size='large'
                weight='light'
              >
                <span ref={this.onSetRef} />
              </Text>
            </Layout>
            <Layout>
              <Text
                size='xsmall'
                weight='light'
                color='gray400'
              >
                До следующего запуска
              </Text>
            </Layout>
          </Row>
        </Layout>
      </Column>
    )
  }
}

export default SessionWait
