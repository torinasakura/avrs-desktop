import { createReducer } from '../utils'
import * as actions from '../constants/security'

const initialState = {}

export default createReducer(initialState, {
  [actions.sync]: (state, { token }) => ({ ...state, token }),
})
