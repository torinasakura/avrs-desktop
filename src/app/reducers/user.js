import { createReducer } from '../utils'
import * as actions from '../constants/user'

const initialState = {}

export default createReducer(initialState, {
  [actions.sync]: (state, { user }) => ({
    ...state,
    ...user,
    isNew: user.status === 'NEW',
  }),
  [actions.update]: (state, { user }) => ({ ...state, ...user }),
  [actions.setServicePlan]: (state, { plan }) => ({ ...state, plan }),
})
