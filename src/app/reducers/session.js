import { createReducer } from '../utils'
import * as actions from '../constants/session'

const initialState = {
  started: false,
  activation: null,
  startAt: null,
  endAt: null,
  nextStartAt: null,
}

export default createReducer(initialState, {
  [actions.started]: (state, { data }) => ({
    ...state,
    started: true,
    ...data,
    nextStartAt: null,
  }),
  [actions.stopped]: () => initialState,
  [actions.wait]: (state, { data }) => ({
    ...state,
    started: true,
    ...data,
    startAt: null,
    endAt: null,
  }),
})
