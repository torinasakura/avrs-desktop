import { combineReducers } from 'redux'
import { routerStateReducer as router } from 'redux-router'
import client from '../api/client'
import user from './user'
import session from './session'
import security from './security'
import auth from '../pages/auth/reducers'
import servicePlans from '../pages/ServicePlans/reducers'
import statistics from '../pages/statistics/reducers'
import schedule from '../pages/schedule/reducers'

export default combineReducers({
  apollo: client.reducer(),
  router,
  user,
  session,
  security,
  auth,
  servicePlans,
  statistics,
  schedule,
})
