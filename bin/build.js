/* eslint-disable no-console */
import webpack from 'webpack'
import * as config from '../config/webpack/app/build'
import * as mainConfig from '../config/webpack/main/build'

webpack([mainConfig, config]).run((error, stats) => {
  if (stats.hasErrors()) {
    console.log(stats.toString({ chunks: false }))

    throw new Error('Compilation error')
  } else if (error) {
    throw error
  }
})
