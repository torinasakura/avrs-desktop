/* eslint-disable no-console */
/* eslint-disable import/no-unresolved */
import path from 'path'
import { spawn } from 'child_process'
import Express from 'express'
import webpack from 'webpack'
import serveStatic from 'serve-static'
import devMiddleware from 'webpack-dev-middleware'
import hotMiddleware from 'webpack-hot-middleware'
import electron from 'electron'
import * as config from '../config/webpack/app/dev'
import * as mainConfig from '../config/webpack/main/dev'

const app = new Express()

const compiler = webpack(config)

app.use(serveStatic(path.resolve(__dirname, '../resources')))

app.use(devMiddleware(compiler, { noInfo: true, publicPath: 'http://localhost:3030/dist/' }))
app.use(hotMiddleware(compiler))

app.listen(3030, (error) => {
  if (error) {
    throw error
  }

  console.info('Development server listening on port %s', 3030)
})

let child = null

webpack(mainConfig).watch({}, (error, stats) => {
  if (stats.hasErrors()) {
    console.log(stats.toString({ chunks: false }))
  }

  if (error) {
    throw error
  }

  if (child) {
    child.kill()
  }

  child = spawn(electron, ['./build'], { stdio: 'inherit' })
})
