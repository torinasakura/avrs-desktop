/* eslint-disable no-console */
import fs from 'fs-extra-promise'
import packager from 'electron-packager'
import { metadata, distDir, packagedBaseDir, packagedDir, iconPath } from './config'

const defaults = {
  dir: distDir,
  out: packagedBaseDir,
  platform: process.platform,
  asar: true,
  prune: true,
  overwrite: true,
  icon: iconPath,
  name: 'Aversis',
  win32metadata: {
    CompanyName: 'Aversis',
    FileDescription: 'Aversis',
    ProductName: 'Aversis',
  },
  'version-string': {
    CompanyName: 'Aversis',
    FileDescription: 'Aversis',
    ProductName: 'Aversis',
  },
  'app-bundle-id': 'net.aversis',
  'app-copyright': `Copyright © ${(new Date()).getFullYear()} Aversis. All rights reserved.`,
  'app-version': metadata.version,
  'build-version': metadata.version,
}

const run = arch =>
  new Promise((resolve, reject) => {
    packager({ ...defaults, arch }, (error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })

const pack = async (arch) => {
  try {
    const [target] = await run(arch)
    await fs.rename(target, packagedDir(arch))
  } catch (error) {
    console.error(error)
  }
}

pack('x64')
