/* eslint-disable no-console */
import path from 'path'
import fs from 'fs-extra-promise'
import { createWindowsInstaller } from 'electron-winstaller'
import { metadata, protectedDir, outputDir, loadingGif, iconPath } from './config'

const replaceTemplate = async () => {
  const templatePath = path.join(__dirname, 'nuspec', 'template.nuspectemplate')
  const replacePath = path.join(__dirname, '..', '..', 'node_modules', 'electron-winstaller', 'template.nuspectemplate')

  await fs.copyAsync(templatePath, replacePath)
}

const createInstaler = async (arch) => {
  try {
    await replaceTemplate()

    await createWindowsInstaller({
      ...metadata,
      loadingGif,
      iconUrl: iconPath,
      usePackageJson: false,
      title: metadata.productName || metadata.name,
      appDirectory: protectedDir(arch),
      outputDirectory: outputDir(arch),
      setupExe: `AversisSetup-${metadata.version}.exe`,
      exe: 'Aversis.exe',
      noMsi: true,
    })

    console.log(`Release created in ${outputDir(arch)}`)
  } catch (error) {
    console.error(error)
  }
}

createInstaler('x64')
