/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
import path from 'path'
import { spawn } from 'child_process'
import fs from 'fs-extra-promise'
import enigma from './enigma'
import { metadata, packagedDir, protectedDir, virtualBox, enigmaExe } from './config'

const processFile = async (target, file) => {
  const stat = await fs.statAsync(path.join(target, file))

  if (stat.isDirectory()) {
    const files = await getFiles(path.join(target, file))

    return { name: file, target, files }
  }

  return { name: file, target }
}

const getFiles = async (target) => {
  const files = await fs.readdirAsync(target)
  const filtered = files.filter(file => !['Aversis.exe'].includes(file))

  return await Promise.all(filtered.map(file => processFile(target, file)))
}

const createConfig = async (arch) => {
  const files = await getFiles(packagedDir(arch))
  const configPath = path.join(protectedDir(arch), '..', 'aversis.enigma64')

  const template = enigma({
    ...metadata,
    inputFileName: path.join(packagedDir(arch), 'Aversis.exe'),
    outputFileName: path.join(protectedDir(arch), 'Aversis.exe'),
    virtualBox: {
      ...virtualBox,
      files,
    },
  })

  await fs.writeFileAsync(configPath, template)

  return configPath
}

const run = async (arch, configPath) =>
  new Promise((resolve, reject) => {
    const proc = spawn(enigmaExe[arch], [configPath], { stdio: 'inherit' })

    proc.on('close', (code) => {
      if (code === 0) {
        resolve()
      } else {
        reject(new Error(`Failed with exit code: ${code}`))
      }
    })
  })

const protect = async (arch) => {
  try {
    await fs.mkdirsAsync(protectedDir(arch))
    const configPath = await createConfig(arch)

    await run(arch, configPath)
  } catch (error) {
    console.error(error)
  }
}

protect('x64')
