/* eslint-disable max-len */
import path from 'path'
import builder from 'xmlbuilder'

const project = () => ({
  EnigmaProject: {
    '@Version': '1.60',
    '@Modification': '530',
  },
})

const compatibility = () => ({
  Compatibility: {
    Level1: 'True',
    Level2: 'True',
    Level3: 'True',
    Level4: 'True',
    Level5: 'True',
    Level6: 'True',
  },
})

const projectDetails = (name, description) => ({
  EnigmaProjectDetails: {
    FullNameOfProject: name,
    ProjectDescription: description,
  },
})

const keysGenerator = () => ({
  KeysGenerator: {
    EnableLogging: 'False',
  },
})

const input = (fileName, productName, version) => ({
  Input: {
    FileName: fileName,
    ProductName: productName,
    VersionInfo: version,
    ProductInfoFromResources: 'False',
  },
})

const output = fileName => ({
  Output: {
    SameAsInputFile: 'False',
    DoNotBackup: 'False',
    FileName: fileName,
  },
})

const advanceInput = () => ({
  AdvanceInput: {
    ProtectAfter: 'False',
    Files: {
      '@Count': 0,
    },
  },
})

const compression = () => ({
  Compression: {
    Type: 'Best',
    Mode: 'AllResourcesExceptIconAndVersion',
  },
})

const registrationFeatures = () => ({
  RegistrationFeatures: {
    RegistrationDataStoring: {
      Storage: 'SystemRegistry',
      Registry: {
        Base: 'CurrentUser',
        Path: '\\SOFTWARE\\EnigmaDevelopers\\',
      },
      FileSystem: {
        Base: 'MyDocumentsFolder',
        Path: '\\EnigmaDevelopers\\license.dat',
        SetFileAttributes: 'False',
        FileAttributesArchive: 'False',
        FileAttributesReadOnly: 'True',
        FileAttributesHidden: 'False',
        FileAttributesSystem: 'False',
      },
      EncryptRegInfo: 'False',
      EncryptWithHardwareId: 'False',
    },
    Common: {
      Unicode: 'False',
      AllowHardwareLockedKeys: 'False',
      AllowTimeLimitedKeys: 'False',
      AllowExecOnlyIfRegistered: 'False',
      KeyMode: 2,
      KeyBase: 3,
      EncryptApplication: 'False',
      AllowRegisterAfterKeys: 'False',
      AllowRegisterBeforeKeys: 'False',
      AllowExecutionsKeys: 'False',
      AllowDaysKeys: 'False',
      AllowRunTimeKeys: 'False',
      AllowGlobalTimeKeys: 'False',
      AllowCountryLockedKeys: 'False',
    },
    HardwareAssociation: {
      UseVolumeSerialDrive: 'False',
      UseSystemVolumeName: 'False',
      UseComputerName: 'False',
      UseCPUType: 'False',
      UseWindowsKey: 'False',
      UseMotherboard: 'False',
      UseHDDSerial: 'True',
      UseUserName: 'False',
      VolumeSerialDriveChange: 0,
      SystemVolumeNameChange: 0,
      ComputerNameChange: 0,
      CPUTypeChange: 0,
      MotherboardChange: 0,
      WindowsKeyChange: 0,
      HDDSerialChange: 0,
      UserNameChange: 0,
    },
    Constants: {
      EncryptedConstant: 457759102,
      Secure1: '5361BB2DC8C1F4AE00844E5BCE74D9B3',
      WatermarkConstant: '6322D255E22B381382EC535C4F576D3E',
      Mode512: {
        PublicKey: null,
        PrivateKey: null,
      },
      Mode768: {
        PublicKey: null,
        PrivateKey: null,
      },
      Mode1024: {
        PublicKey: '020D0C62EC038C295998CF59B820BF5F4AB0CDCYFBBK7ZJ6P5JRWZ836R556GVDNVCQMZLHZCBLST4VFAUCM2JMQHLQA5TGYPHKDJGNXEHGKNVUSJBTZJW2UG5CG669TYWNB7GRUYQ86QWX5RX7YT4B7M5KQ86Z5D76FTCRMHP8TFQS9SYUFMPRXMMDPHJ86WR95YETAZBG5CE8USGUNP9Z77RPM2R6Q4DQ5U45AWG2RAAW6S50CDG47ZYCF4NYAWP5DQX3ERV8XH6MZ9NS4X4RG5ECN6RZY4CBMJ3HLBMB4EA9GE6X95JNEKS9D7QYBUBBN7KMN4EFLWJLZA8FTKGPLQ75PACCHSLS4CM7ZZ9Y7M3NF6GXDHMJH6L3KN3ZGAT7JGWPGDFK5P78B652Z5SPHHQH6WCJMEUV4UUQD977WCN3YMN4VRTT49V4EJ2KDTF0CDKTTQ6LWLTGTJ8URTVYJVQ5RR8K33ZWJTTLSED7PC8A7XSFWSR284SYCU9KT8BPY2RAR9YG25DMCGXKJFGZZZKK3WEJ2HGGKZU6CCQX2L3ZCPHRD7UEVMHCTUTQV33YFZ94LS836RA6SQV9LF3P7JCQBS7APHPQRKW52B3EM2M92R5YCZ8ZQGZ9N9PAM5ETLMP9JWJZDNZVF59',
        PrivateKey: '020445REZAYR3XFDX3LPLZYGEE34PU642YA0ADL4FKYM82D3M65GCM7KG2YRS2YM8A9T3UASNMY3GW532X7UGR4EK8Y55QQ6TEUPK5PTKXD8KVUJP4KHPTSQT9NN889WP9D7Y8FV4RFZMNPSZ53BHA336NN33WUK9NLTNV2TJPGXADFLTGHCFV4Q2HVF75VS65WMD43Y6J7BAPY2GBC0206H47SVH4U9QFV9675K8AEEUHMAHQW4YR0206H47SVH4U9QFV9675K8AEEUHMAHQW4YR',
      },
      Mode2048: {
        PublicKey: null,
        PrivateKey: null,
      },
      Mode3072: {
        PublicKey: null,
        PrivateKey: null,
      },
      Mode4096: {
        PublicKey: null,
        PrivateKey: null,
      },
      HardwareConstant: 'D0C62EC038C295998CF59B820BF5F4AB',
    },
  },
})

const vBoxFile = ({ name, target, files }) => {
  if (files) {
    return {
      Type: 3,
      Name: name,
      Action: 0,
      OverwriteDateTime: 'False',
      OverwriteAttributes: 'False',
      Files: {
        File: files.map(vBoxFile),
      },
    }
  }

  return {
    Type: 2,
    Name: name,
    File: path.join(target, name),
    ActiveX: 'False',
    ActiveXInstall: 'False',
    Action: 0,
    OverwriteDateTime: 'False',
    OverwriteAttributes: 'False',
    PassCommandLine: 'False',
  }
}

const vBox = ({ secure1, secure2, privateKey, publicKey, files }) => ({
  VirtualBox: {
    Secure1: secure1,
    Secure2: secure2,
    PrivateKey: privateKey,
    PublicKey: publicKey,
    Files: {
      Enabled: 'True',
      DeleteExtractedOnExit: 'False',
      CompressFiles: 'False',
      Files: {
        File: {
          Type: 3,
          Name: '%DEFAULT FOLDER%',
          Action: 0,
          OverwriteDateTime: 'False',
          OverwriteAttributes: 'False',
          Files: {
            File: files.map(vBoxFile),
          },
        },
      },
    },
    Options: {
      MapExecutableWithTemporaryFile: 'True',
      ShareVirtualSystemToChildProcesses: 'True',
      AllowRunningOfVirtualExeFiles: 'True',
    },
  },
})

export default ({
  name, description, productName, version,
  inputFileName, outputFileName, virtualBox,
} = {}) =>
  builder.create(project(), { encoding: 'UTF-8' })
         .ele(compatibility())
         .insertAfter(projectDetails(name, description))
         .insertAfter(keysGenerator())
         .insertAfter(input(inputFileName, productName, version))
         .insertAfter(output(outputFileName))
         .insertAfter(advanceInput())
         .insertAfter(compression())
         .insertAfter(registrationFeatures())
         .insertAfter(vBox(virtualBox))
         .end({ pretty: true })
