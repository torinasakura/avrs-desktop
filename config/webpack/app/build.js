import path from 'path'
import webpack from 'webpack'
import nested from 'jss-nested'
import camelCase from 'jss-camel-case'
import autoprefixer from 'autoprefixer'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import CssResolvePlugin from 'elementum-tools/lib/webpack/css-resolve-plugin'

export const target = 'electron-renderer'

export const entry = {
  auth: [
    'babel-polyfill',
    './src/app/auth.js',
  ],
  main: [
    'babel-polyfill',
    './src/app/main.js',
  ],
}

export const output = {
  filename: '[name].js',
  path: './build/public/',
  publicPath: 'aversis://aversis/',
}

export const module = {
  rules: [
    {
      test: /\.js?$/,
      exclude: /node_modules\/(?!avrs-ui)/,
      loader: 'babel-loader',
      options: {
        babelrc: false,
        presets: [
          ['es2015', { modules: false }],
          'stage-0',
          'react',
        ],
        plugins: [
          ['elementum-tools/lib/babel/plugin', {
            alias: {
              AvrsDesktop: 'src/app',
              AvrsUI: 'node_modules/avrs-ui/src',
            },
            extract: true,
          }],
        ],
      },
    },
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: [
          'css-loader',
          'postcss-loader',
        ],
      }),
    },
    {
      test: /\.jss$/,
      loader: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: [
          'css-loader',
          'postcss-loader',
          'jss-loader',
        ],
      }),
    },
    {
      test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
      loader: 'file-loader?name=[path][name].[ext]',
    },
  ],
}

export const resolve = {
  plugins: [
    new CssResolvePlugin(),
  ],
}

export const plugins = [
  new ExtractTextPlugin('[name].css'),
  new HtmlWebpackPlugin({
    chunks: ['auth'],
    filename: 'auth.html',
    template: path.resolve(__dirname, 'index.ejs'),
  }),
  new HtmlWebpackPlugin({
    chunks: ['main'],
    filename: 'main.html',
    template: path.resolve(__dirname, 'index.ejs'),
  }),
  new webpack.DefinePlugin({
    'process.env.CABINET_URL': JSON.stringify('http://cabinet.stage.aversis.net/'),
    'process.env.API_URL': JSON.stringify('http://api.stage.aversis.net/'),
    'process.env.NODE_ENV': JSON.stringify('production'),
  }),
  new webpack.LoaderOptionsPlugin({
    options: {
      jssLoader: {
        plugins: [
          nested(),
          camelCase(),
        ],
      },
      postcss: {
        plugins: autoprefixer({
          browsers: [
            '>2%',
            'last 2 versions',
          ],
        }),
      },
    },
  }),
  new webpack.optimize.UglifyJsPlugin(),
  new CopyWebpackPlugin([{
    from: path.join(__dirname, '..', '..', '..', 'resources', 'fonts'),
    to: 'fonts',
  }]),
]
