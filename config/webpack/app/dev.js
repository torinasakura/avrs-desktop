import path from 'path'
import webpack from 'webpack'
import nested from 'jss-nested'
import camelCase from 'jss-camel-case'
import autoprefixer from 'autoprefixer'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CssResolvePlugin from 'elementum-tools/lib/webpack/css-resolve-plugin'

export const target = 'electron-renderer'

export const entry = {
  auth: [
    'babel-polyfill',
    'webpack-hot-middleware/client?path=http://localhost:3030/__webpack_hmr',
    'react-hot-loader/patch',
    './src/app/auth.js',
  ],
  main: [
    'babel-polyfill',
    'webpack-hot-middleware/client?path=http://localhost:3030/__webpack_hmr',
    'react-hot-loader/patch',
    './src/app/main.js',
  ],
}

export const output = {
  filename: '[name].js',
  publicPath: 'http://localhost:3030/dist/',
}

export const module = {
  rules: [
    {
      test: /\.js?$/,
      exclude: /node_modules\/(?!avrs-ui)/,
      use: [
        {
          loader: 'react-hot-loader/webpack',
        },
        {
          loader: 'babel-loader',
          query: {
            babelrc: false,
            presets: [
              ['es2015', { modules: false }],
              'stage-0',
              'react',
            ],
            plugins: [
              ['elementum-tools/lib/babel/plugin', {
                alias: {
                  AvrsDesktop: 'src/app',
                  AvrsUI: 'node_modules/avrs-ui/src',
                },
                extract: true,
              }],
            ],
          },
        },
      ],
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader',
        'postcss-loader',
      ],
    },
    {
      test: /\.jss$/,
      use: [
        'style-loader',
        'css-loader',
        'postcss-loader',
        'jss-loader',
      ],
    },
    {
      test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
      loader: 'file-loader?name=[path][name].[ext]',
    },
  ],
}

export const resolve = {
  plugins: [
    new CssResolvePlugin(),
  ],
}

export const plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new HtmlWebpackPlugin({
    chunks: ['auth'],
    filename: 'auth.html',
    template: path.resolve(__dirname, 'index.ejs'),
  }),
  new HtmlWebpackPlugin({
    chunks: ['main'],
    filename: 'main.html',
    template: path.resolve(__dirname, 'index.ejs'),
  }),
  new webpack.DefinePlugin({
    'process.env.CABINET_URL': JSON.stringify('http://cabinet.stage.aversis.net/'),
    'process.env.API_URL': JSON.stringify(process.env.API_URL || 'http://api.stage.aversis.net/'),
  }),
  new webpack.LoaderOptionsPlugin({
    options: {
      jssLoader: {
        plugins: [
          nested(),
          camelCase(),
        ],
      },
      postcss: {
        plugins: autoprefixer({
          browsers: [
            '>2%',
            'last 2 versions',
          ],
        }),
      },
    },
  }),
]
