import path from 'path'
import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { version } from '../../../package.json'

export const target = 'electron-main'

export const entry = [
  'babel-polyfill',
  './src/main/index.js',
]

export const output = {
  filename: 'index.js',
  path: './build/',
}

export const module = {
  rules: [
    {
      test: /\.js?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        babelrc: false,
        presets: [
          'es2015',
          'stage-0',
        ],
      },
    },
    {
      test: /\.json?$/,
      loader: 'json-loader',
    },
    {
      test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
      loader: 'file-loader?name=[name].[ext]',
    },
  ],
}

export const plugins = [
  new webpack.DefinePlugin({
    'process.env.API_URL': JSON.stringify(process.env.API_URL || 'http://api.stage.aversis.net/'),
  }),
  new HtmlWebpackPlugin({
    inject: false,
    filename: 'package.json',
    template: path.resolve(__dirname, 'package.ejs'),
    version,
  }),
]
